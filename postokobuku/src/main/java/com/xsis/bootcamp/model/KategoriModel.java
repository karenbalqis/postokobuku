package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="KATEGORI")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")

public class KategoriModel {
	private Integer id;
	private String nama;
	private String keterangan;
	private Set<BukuModel> bukuModels;


	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "KATEGORI")
	@TableGenerator(name = "KATEGORI", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "KATEGORI", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAMA_KATEGORI")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Column(name = "KETERANGAN")
	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "kategoriModel")
	public Set<BukuModel> getBukuModels() {
		return bukuModels;
	}
	
	public void setBukuModels(Set<BukuModel> bukuModels) {
		this.bukuModels = bukuModels;
	}
	
	
}
