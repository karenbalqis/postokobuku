package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "PENJUALAN_DETAIL")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class PenjualanDetailModel {
	private Integer id;
	private Integer bukuId; //
	private Double harga; //
	private Integer jumlah; //
	private Double subTotal; //
	private Integer penjualanId;

	// Constraint with Penjualan as child
	private PenjualanModel penjualanModel;

	// Constraint with buku as child
	private BukuModel bukuModel;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PENJUALAN_DETAIL")
	@TableGenerator(name = "PENJUALAN_DETAIL", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "PENJUALAN_DETAIL", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "BUKU_ID")
	public Integer getBukuId() {
		return bukuId;
	}

	public void setBukuId(Integer bukuId) {
		this.bukuId = bukuId;
	}

	@Column(name = "HARGA")
	public Double getHarga() {
		return harga;
	}

	public void setHarga(Double harga) {
		this.harga = harga;
	}

	@Column(name = "JUMLAH")
	public Integer getJumlah() {
		return jumlah;
	}

	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}

	@Column(name = "SUB_TOTAL")
	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	@Column(name = "PENJUALAN_ID")
	public Integer getPenjualanId() {
		return penjualanId;
	}

	public void setPenjualanId(Integer penjualanId) {
		this.penjualanId = penjualanId;
	}
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PENJUALAN_ID", nullable = false, insertable = false, updatable = false)
	public PenjualanModel getPenjualanModel() {
		return penjualanModel;
	}

	public void setPenjualanModel(PenjualanModel penjualanModel) {
		this.penjualanModel = penjualanModel;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "BUKU_ID", nullable = false, insertable = false, updatable = false)
	public BukuModel getBukuModel() {
		return bukuModel;
	}

	public void setBukuModel(BukuModel bukuModels) {
		this.bukuModel = bukuModels;
	}

}
