package com.xsis.bootcamp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "STOK")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class StokModel {
	private Integer id;
	private Integer bukuId;
	private Date tanggal;
	private Integer jumlahStok;
	
	//Constraint
	private BukuModel bukuModel;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "STOK")
	@TableGenerator(name = "STOK", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "STOK", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "BUKU_ID")
	public Integer getBukuId() {
		return bukuId;
	}

	public void setBukuId(Integer bukuId) {
		this.bukuId = bukuId;
	}
	
	@Column(name = "TANGGAL")
	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
	@Column(name = "JUMLAH")
	public Integer getJumlahStok() {
		return jumlahStok;
	}

	public void setJumlahStok(Integer jumlahStok) {
		this.jumlahStok = jumlahStok;
	}
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.REMOVE, optional=true)
	@JoinColumn(name = "BUKU_ID", nullable = false, updatable = false, insertable = false)
	public BukuModel getBukuModel() {
		return bukuModel;
	}

	public void setBukuModel(BukuModel bukuModel) {
		this.bukuModel = bukuModel;
	}
	
}
