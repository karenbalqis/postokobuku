package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "PEMBELIAN")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class PembelianModel {
	private Integer id;
	private String nomorFaktur;
	private Date tanggalBeli;
	private Integer supplierId;
	private Double total;
	private Integer userId;
	
	//Constraint with detail pembelian as parent
	private List<PembelianDetailModel> detailPembelians;
	
	//Constraint with supplier as child
	private SupplierModel supplierModel;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PEMBELIAN")
	@TableGenerator(name = "PEMBELIAN", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "PEMBELIAN", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "NOMOR_FAKTUR")
	public String getNomorFaktur() {
		return nomorFaktur;
	}

	public void setNomorFaktur(String nomorFaktur) {
		this.nomorFaktur = nomorFaktur;
	}
	
	@Column(name = "TANGGAL_BELI")
	public Date getTanggalBeli() {
		return tanggalBeli;
	}

	public void setTanggalBeli(Date tanggalBeli) {
		this.tanggalBeli = tanggalBeli;
	}
	
	@Column(name = "SUPPLIER_ID")
	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}
	
	@Column(name = "TOTAL")
	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
	@Column(name = "USER_ID")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pembelianModel")
	public List<PembelianDetailModel> getDetailPembelians() {
		return detailPembelians;
	}

	public void setDetailPembelians(List<PembelianDetailModel> detailPembelians) {
		this.detailPembelians = detailPembelians;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SUPPLIER_ID", nullable = false, insertable = false, updatable = false)
	public SupplierModel getSupplierModel() {
		return supplierModel;
	}

	public void setSupplierModel(SupplierModel supplierModel) {
		this.supplierModel = supplierModel;
	}
	
}
