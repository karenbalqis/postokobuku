package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "PENJUALAN")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class PenjualanModel {
	private Integer id;
	private String nomorFaktur;
	private Date tanggalBeli;
	private Integer pelangganId;
	private Integer total;
	private Integer userId;

	// Constraint with detail penjualan as parent
	private List<PenjualanDetailModel> penjualanDetailModels;

	// Constraint with supplier as child
	private PelangganModel pelangganModel;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PENERBIT")
	@TableGenerator(name = "PENERBIT", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "PENERBIT", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NO_FAKTUR")
	public String getNomorFaktur() {
		return nomorFaktur;
	}

	public void setNomorFaktur(String nomorFaktur) {
		this.nomorFaktur = nomorFaktur;
	}

	@Column(name = "TANGGAL_BELI")
	public Date getTanggalBeli() {
		return tanggalBeli;
	}

	public void setTanggalBeli(Date tanggalBeli) {
		this.tanggalBeli = tanggalBeli;
	}

	@Column(name = "PELANGGAN_ID")
	public Integer getpelangganId() {
		return pelangganId;
	}

	public void setpelangganId(Integer pelangganId) {
		this.pelangganId = pelangganId;
	}

	@Column(name = "TOTAL")
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Column(name = "USER_ID")
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PELANGGAN_ID", nullable = false, insertable = false, updatable = false)
	public PelangganModel getPelangganModel() {
		return pelangganModel;
	}

	public void setPelangganModel(PelangganModel pelangganModel) {
		this.pelangganModel = pelangganModel;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "penjualanModel")
	public List<PenjualanDetailModel> getPenjualanDetailModels() {
		return penjualanDetailModels;
	}

	public void setPenjualanDetailModels(List<PenjualanDetailModel> penjualanDetailModels) {
		this.penjualanDetailModels = penjualanDetailModels;
	}

}
