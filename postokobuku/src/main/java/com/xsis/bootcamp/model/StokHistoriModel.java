package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "STOK_HISTORI")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class StokHistoriModel {
	private Integer id;
	private Integer bukuId;
	private Integer jumlahBeli;
	private Integer jumlahJual;
	private Integer stokAkhir;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "STOK_HISTORI")
	@TableGenerator(name = "STOK_HISTORI", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "STOK_HISTORI", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "BUKU_ID")
	public Integer getBukuId() {
		return bukuId;
	}

	public void setBukuId(Integer bukuId) {
		this.bukuId = bukuId;
	}
	
	@Column(name = "JUMLAH_BELI")
	public Integer getJumlahBeli() {
		return jumlahBeli;
	}

	public void setJumlahBeli(Integer jumlahBeli) {
		this.jumlahBeli = jumlahBeli;
	}
	
	@Column(name = "JUMLAH_JUAL")
	public Integer getJumlahJual() {
		return jumlahJual;
	}

	public void setJumlahJual(Integer jumlahJual) {
		this.jumlahJual = jumlahJual;
	}
	
	@Column(name = "STOK_AKHIR")
	public Integer getStokAkhir() {
		return stokAkhir;
	}

	public void setStokAkhir(Integer stokAkhir) {
		this.stokAkhir = stokAkhir;
	}

}
