package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "ROLEUSER")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class RoleModel {

	private Integer id;
	private String namaRole;
	private String keterangan;
	private Set<UserModel> userModel;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ROLEUSER")
	@TableGenerator(name = "ROLEUSER", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "ROLEUSER", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAMA_ROLE")
	public String getNamaRole() {
		return namaRole;
	}

	public void setNamaRole(String namaRole) {
		this.namaRole = namaRole;
	}

	@Column(name = "KETERANGAN")
	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "roleModel")
	public Set<UserModel> getUserModel() {
		return userModel;
	}

	public void setUserModel(Set<UserModel> userModel) {
		this.userModel = userModel;
	}

}
