package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PENGARANG")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class PengarangModel {
	private Integer id;
	private String namaPengarang;
	private String alamat;
	private Integer provinsiId;
	private Integer kotaId;
	private Integer kecamatanId;
	private String noTelepon;
	private String email;
	
	private Set<BukuModel> bukuModels;	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PENGARANG")
	@TableGenerator(name = "PENGARANG", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "PENGARANG", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}	
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "NAMA_PENGARANG")
	public String getNamaPengarang() {
		return namaPengarang;
	}	
	public void setNamaPengarang(String namaPengarang) {
		this.namaPengarang = namaPengarang;
	}
	
	@Column(name = "ALAMAT")
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@Column(name = "PROVINSI_ID")
	public Integer getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(Integer provinsiId) {
		this.provinsiId = provinsiId;
	}
	
	@Column(name = "KOTA_ID")
	public Integer getKotaId() {
		return kotaId;
	}
	public void setKotaId(Integer kotaId) {
		this.kotaId = kotaId;
	}
	
	@Column(name = "KECAMATAN_ID")
	public Integer getKecamatanId() {
		return kecamatanId;
	}	
	public void setKecamatanId(Integer kecamatanId) {
		this.kecamatanId = kecamatanId;
	}
	
	@Column(name = "NO_TELP")
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	
	@Column(name = "EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	// nama intasiasi PengarangModel di BukuModel = pengarangModel
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pengarangModel")
	public Set<BukuModel> getBukuModels() {
		return bukuModels;
	}
	
	public void setBukuModels(Set<BukuModel> bukuModels) {
		this.bukuModels = bukuModels;
	}
	
}
