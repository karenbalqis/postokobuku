package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author TechnoCore
 *
 */
@Entity
@Table(name = "PEMBELIAN_DETAIL")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class PembelianDetailModel {
	private Integer id;
	private Integer bukuId;
	private Double harga;
	private Integer jumlah;
	private Double subTotal;
	private Integer pembelianId;

	// Constraint with pembelian as child
	private PembelianModel pembelianModel;
	
	// Constraint with buku as child
	private BukuModel bukuModel;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PEMBELIAN_DETAIL")
	@TableGenerator(name = "PEMBELIAN_DETAIL", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "PEMBELIAN_DETAIL", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "BUKU_ID")
	public Integer getBukuId() {
		return bukuId;
	}

	public void setBukuId(Integer bukuId) {
		this.bukuId = bukuId;
	}

	@Column(name = "HARGA")
	public Double getHarga() {
		return harga;
	}

	public void setHarga(Double harga) {
		this.harga = harga;
	}

	@Column(name = "JUMLAH")
	public Integer getJumlah() {
		return jumlah;
	}

	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}

	@Column(name = "SUB_TOTAL")
	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	@Column(name = "PEMBELIAN_ID")
	public Integer getPembelianId() {
		return pembelianId;
	}

	public void setPembelianId(Integer pembelianId) {
		this.pembelianId = pembelianId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PEMBELIAN_ID", nullable = false, insertable = false, updatable = false)
	public PembelianModel getPembelianModel() {
		return pembelianModel;
	}

	public void setPembelianModel(PembelianModel pembelianModel) {
		this.pembelianModel = pembelianModel;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "BUKU_ID", nullable = false, insertable = false, updatable = false)
	public BukuModel getBukuModel() {
		return bukuModel;
	}

	public void setBukuModel(BukuModel bukuModel) {
		this.bukuModel = bukuModel;
	}
	
	
}
