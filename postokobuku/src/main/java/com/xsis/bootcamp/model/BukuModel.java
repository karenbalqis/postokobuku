package com.xsis.bootcamp.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "BUKU")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class BukuModel {
	// Non Constraint Field
	private Integer id;
	private String judulBuku;
	private String isbn;
	private Integer kategoriId;
	private Integer pengarangId;
	private Integer tahunTerbit;
	private Integer kotaId;
	private Integer penerbitId;
	private Integer jumlahHalaman;
	private Double hargaJual;
	private Double hargaBeli;

	// Constraint Field as Child
	private KategoriModel kategoriModel;
	private PengarangModel pengarangModel;
	private KotaModel kotaModel;
	private PenerbitModel penerbitModel;
	
	// Constraint Field as Parent
	private StokModel stokModel;
	
	//Constraint with penjualan Detail as parent
	private List<PenjualanDetailModel> penjualanDetailModel;
	
	// Constraint with Pembelian Detail as parent
	private Set<PembelianDetailModel> pembelianDetailModels;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "BUKU")
	@TableGenerator(name = "BUKU", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "BUKU", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "JUDUL_BUKU")
	public String getJudulBuku() {
		return judulBuku;
	}

	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}

	@Column(name = "ISBN")
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Column(name = "KATEGORI_ID")
	public Integer getKategoriId() {
		return kategoriId;
	}

	public void setKategoriId(Integer kategoriId) {
		this.kategoriId = kategoriId;
	}

	@Column(name = "PENGARANG_ID")
	public Integer getPengarangId() {
		return pengarangId;
	}

	public void setPengarangId(Integer pengarangId) {
		this.pengarangId = pengarangId;
	}

	@Column(name = "TAHUN_TERBIT")
	public Integer getTahunTerbit() {
		return tahunTerbit;
	}

	public void setTahunTerbit(Integer tahunTerbit) {
		this.tahunTerbit = tahunTerbit;
	}

	@Column(name = "KOTA_ID")
	public Integer getKotaId() {
		return kotaId;
	}

	public void setKotaId(Integer kotaId) {
		this.kotaId = kotaId;
	}

	@Column(name = "PENERBIT_ID")
	public Integer getPenerbitId() {
		return penerbitId;
	}

	public void setPenerbitId(Integer penerbitId) {
		this.penerbitId = penerbitId;
	}

	@Column(name = "JUMLAH_HALAMAN")
	public Integer getJumlahHalaman() {
		return jumlahHalaman;
	}

	public void setJumlahHalaman(Integer jumlahHalaman) {
		this.jumlahHalaman = jumlahHalaman;
	}
	
	@Column(name = "HARGA_JUAL")
	public Double getHargaJual() {
		return hargaJual;
	}

	public void setHargaJual(Double hargaJual) {
		this.hargaJual = hargaJual;
	}
	
	@Column(name = "HARGA_BELI")
	public Double getHargaBeli() {
		return hargaBeli;
	}

	public void setHargaBeli(Double hargaBeli) {
		this.hargaBeli = hargaBeli;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "KATEGORI_ID", nullable = false, insertable = false, updatable = false)
	public KategoriModel getKategoriModel() {
		return kategoriModel;
	}

	public void setKategoriModel(KategoriModel kategori) {
		this.kategoriModel = kategori;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "KOTA_ID", nullable = false, insertable = false, updatable = false)
	public KotaModel getKotaModel() {
		return kotaModel;
	}

	public void setKotaModel(KotaModel kota) {
		this.kotaModel = kota;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PENGARANG_ID", nullable = false, insertable = false, updatable = false)
	public PengarangModel getPengarangModel() {
		return pengarangModel;
	}

	public void setPengarangModel(PengarangModel pengarang) {
		this.pengarangModel = pengarang;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PENERBIT_ID", nullable = false, insertable = false, updatable = false)
	public PenerbitModel getPenerbitModel() {
		return penerbitModel;
	}

	public void setPenerbitModel(PenerbitModel penerbitModel) {
		this.penerbitModel = penerbitModel;
	}
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "bukuModel")
	public StokModel getStokModel() {
		return stokModel;
	}	
	
	public void setStokModel(StokModel stokModel) {
		this.stokModel = stokModel;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "bukuModel")
	public Set<PembelianDetailModel> getPembelianDetailModels() {
		return pembelianDetailModels;
	}

	public void setPembelianDetailModels(Set<PembelianDetailModel> pembelianDetailModels) {
		this.pembelianDetailModels = pembelianDetailModels;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="bukuModel")
	public List<PenjualanDetailModel> getPenjualanDetailModel() {
		return penjualanDetailModel;
	}

	public void setPenjualanDetailModel(List<PenjualanDetailModel> penjualanDetailModel) {
		this.penjualanDetailModel = penjualanDetailModel;
	}
	
	
	
	
}
