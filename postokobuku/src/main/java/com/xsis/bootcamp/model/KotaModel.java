package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="KOTA")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class KotaModel {
	private Integer id;
	private String nama;
	private Integer provinsiId;
	
	private Set<BukuModel> bukuModels;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="KOTA")
	@TableGenerator(name="KOTA",table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="KOTA", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NAMA_KOTA")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="PROVINSI_ID")
	public Integer getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(Integer provinsiId) {
		this.provinsiId = provinsiId;
	}
	
	//nama intasiasi KotaModel di BukuModel adalah "kotaModel"
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "kotaModel")
	public Set<BukuModel> getBuku() {
		return bukuModels;
	}
	
	public void setBuku(Set<BukuModel> bukuModels) {
		this.bukuModels = bukuModels;
	}
}
