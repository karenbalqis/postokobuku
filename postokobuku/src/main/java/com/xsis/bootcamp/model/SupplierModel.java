package com.xsis.bootcamp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SUPPLIER")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class SupplierModel {
	private Integer id;
	private String namaSupplier;
	private String alamat;
	private Integer provinsiId;
	private Integer kotaId;
	private Integer kecamatanId;
	private String noTelepon;
	private String email;
	private String noFax;
	
	//Constraint with pembelian as parent
	Set<PembelianModel> pembelianModels;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SUPPLIER")
	@TableGenerator(name = "SUPPLIER", table = "SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "SUPPLIER", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAMA_SUPPLIER")
	public String getNamaSupplier() {
		return namaSupplier;
	}

	public void setNamaSupplier(String namaSupplier) {
		this.namaSupplier = namaSupplier;
	}

	@Column(name = "ALAMAT")
	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	@Column(name = "PROVINSI_ID")
	public Integer getProvinsiId() {
		return provinsiId;
	}

	public void setProvinsiId(Integer provinsiId) {
		this.provinsiId = provinsiId;
	}

	@Column(name = "KOTA_ID")
	public Integer getKotaId() {
		return kotaId;
	}

	public void setKotaId(Integer kotaId) {
		this.kotaId = kotaId;
	}

	@Column(name = "KECAMATAN_ID")
	public Integer getKecamatanId() {
		return kecamatanId;
	}

	public void setKecamatanId(Integer kecamatanId) {
		this.kecamatanId = kecamatanId;
	}

	@Column(name = "NO_TELP")
	public String getNoTelepon() {
		return noTelepon;
	}

	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}

	@Column(name = "EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "NO_FAX")
	public String getNoFax() {
		return noFax;
	}

	public void setNoFax(String noFax) {
		this.noFax = noFax;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "supplierModel")
	public Set<PembelianModel> getPembelianModels() {
		return pembelianModels;
	}

	public void setPembelianModels(Set<PembelianModel> pembelianModels) {
		this.pembelianModels = pembelianModels;
	}
}
