/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.BukuDao;
import com.xsis.bootcamp.model.BukuModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class BukuDaoImpl implements BukuDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<BukuModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<BukuModel> result = session.createQuery("from BukuModel").list();
		
		return result;
	}

	
	@Override
	public void insert(BukuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	
	@Override
	public BukuModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		BukuModel result = session.get(BukuModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(BukuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(BukuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
