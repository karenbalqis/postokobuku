package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.BukuModel;

public interface BukuDao {
	public List<BukuModel> get() throws Exception;
	
	public void insert(BukuModel model) throws Exception;
	public BukuModel getById(Integer id) throws Exception;
	public void update(BukuModel model) throws Exception;
	public void delete(BukuModel model) throws Exception;
}
