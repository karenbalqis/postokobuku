/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PelangganDao;
import com.xsis.bootcamp.model.PelangganModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class PelangganDaoImpl implements PelangganDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PelangganModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PelangganModel> result = session.createQuery("from PelangganModel").list();
		
		return result;
	}

	
	@Override
	public void insert(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	
	@Override
	public PelangganModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PelangganModel result = session.get(PelangganModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(PelangganModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
