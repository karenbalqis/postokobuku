/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.StokDao;
import com.xsis.bootcamp.model.StokModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class StokDaoImpl implements StokDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<StokModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<StokModel> result = session.createQuery("from StokModel").list();
		
		return result;
	}
	
	@Override
	public void insert(StokModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	
	@Override
	public StokModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		StokModel result = session.get(StokModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(StokModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(StokModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
