package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PenerbitDao;
import com.xsis.bootcamp.model.PenerbitModel;

@Repository
public class PenerbitDaoImpl implements PenerbitDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<PenerbitModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PenerbitModel> result = session.createQuery("from PenerbitModel").list();
		return result;
	}

	@Override
	public void insert(PenerbitModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public PenerbitModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PenerbitModel result = session.get(PenerbitModel.class, id);
		return result;
	}

	@Override
	public void update(PenerbitModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);

	}

	@Override
	public void delete(PenerbitModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}

	@Override
	public List<PenerbitModel> getByPropinsiId(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenerbitModel> getByKotaId(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PenerbitModel> getByKecamatanId(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
