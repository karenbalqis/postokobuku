package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.StokModel;

public interface StokDao {
	public List<StokModel> get() throws Exception;
	
	public void insert(StokModel model) throws Exception;
	public StokModel getById(Integer id) throws Exception;
	public void update(StokModel model) throws Exception;
	public void delete(StokModel model) throws Exception;
}
