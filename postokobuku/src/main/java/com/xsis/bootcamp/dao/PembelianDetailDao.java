package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PembelianDetailModel;

public interface PembelianDetailDao {
	public List<PembelianDetailModel> get() throws Exception;
	
	public void insert(PembelianDetailModel model) throws Exception;
	public PembelianDetailModel getById(Integer id) throws Exception;
	public void update(PembelianDetailModel model) throws Exception;
	public void delete(PembelianDetailModel model) throws Exception;
}
