/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.StokHistoriDao;
import com.xsis.bootcamp.model.StokHistoriModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class StokHistoriDaoImpl implements StokHistoriDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<StokHistoriModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<StokHistoriModel> result = session.createQuery("from StokHistoriModel").list();
		
		return result;
	}

	
	@Override
	public void insert(StokHistoriModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	
	@Override
	public StokHistoriModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		StokHistoriModel result = session.get(StokHistoriModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(StokHistoriModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(StokHistoriModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
