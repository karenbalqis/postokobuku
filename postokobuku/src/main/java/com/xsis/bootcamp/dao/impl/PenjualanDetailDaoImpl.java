/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PenjualanDetailDao;
import com.xsis.bootcamp.model.PenjualanDetailModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class PenjualanDetailDaoImpl implements PenjualanDetailDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PenjualanDetailModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PenjualanDetailModel> result = session.createQuery("from PenjualanDetailModel").list();
		
		return result;
	}

	
	@Override
	public void insert(PenjualanDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);	
	}

	
	@Override
	public PenjualanDetailModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PenjualanDetailModel result = session.get(PenjualanDetailModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(PenjualanDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(PenjualanDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
