package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PembelianModel;

public interface PembelianDao {
	public List<PembelianModel> get() throws Exception;
	
	public void insert(PembelianModel model) throws Exception;
	public PembelianModel getById(Integer id) throws Exception;
	public void update(PembelianModel model) throws Exception;
	public void delete(PembelianModel model) throws Exception;
}
