/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PengarangDao;
import com.xsis.bootcamp.model.PengarangModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class PengarangDaoImpl implements PengarangDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PengarangModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PengarangModel> result = session.createQuery("from PengarangModel").list();
		
		return result;
	}

	
	@Override
	public void insert(PengarangModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	
	@Override
	public PengarangModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PengarangModel result = session.get(PengarangModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(PengarangModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(PengarangModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
