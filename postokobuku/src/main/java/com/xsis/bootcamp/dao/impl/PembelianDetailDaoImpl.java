/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PembelianDetailDao;
import com.xsis.bootcamp.model.PembelianDetailModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class PembelianDetailDaoImpl implements PembelianDetailDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PembelianDetailModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PembelianDetailModel> result = session.createQuery("from PembelianDetailModel").list();
		
		return result;
	}

	
	@Override
	public void insert(PembelianDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);	
	}

	
	@Override
	public PembelianDetailModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PembelianDetailModel result = session.get(PembelianDetailModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(PembelianDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(PembelianDetailModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
