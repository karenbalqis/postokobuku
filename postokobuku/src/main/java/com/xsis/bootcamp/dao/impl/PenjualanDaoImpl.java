package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PenjualanDao;
import com.xsis.bootcamp.model.PenjualanModel;

@Repository
public class PenjualanDaoImpl implements PenjualanDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PenjualanModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PenjualanModel> result = session.createQuery("from PenjualanModel").list();
		
		return result;
	}

	@Override
	public void insert(PenjualanModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);

	}

	@Override
	public PenjualanModel getById(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PenjualanModel result = session.get(PenjualanModel.class, id);
		
		return result;
	}

	@Override
	public void update(PenjualanModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);

	}

	@Override
	public void delete(PenjualanModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);

	}

}
