package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.StokHistoriModel;

public interface StokHistoriDao {
	public List<StokHistoriModel> get() throws Exception;
	
	public void insert(StokHistoriModel model) throws Exception;
	public StokHistoriModel getById(Integer id) throws Exception;
	public void update(StokHistoriModel model) throws Exception;
	public void delete(StokHistoriModel model) throws Exception;
}
