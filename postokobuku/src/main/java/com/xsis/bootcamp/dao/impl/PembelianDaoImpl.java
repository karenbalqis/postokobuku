/**
 * 
 */
package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PembelianDao;
import com.xsis.bootcamp.model.PembelianDetailModel;
import com.xsis.bootcamp.model.PembelianModel;

/**
 * @author TechnoCore
 *
 */

@Repository
public class PembelianDaoImpl implements PembelianDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PembelianModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<PembelianModel> result = session.createQuery("from PembelianModel").list();
		
		return result;
	}

	
	@Override
	public void insert(PembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);		
	}

	
	@Override
	public PembelianModel getById(Integer  id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		PembelianModel result = session.get(PembelianModel.class, id);
		
		return result;
	}

	
	@Override
	public void update(PembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	
	@Override
	public void delete(PembelianModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
