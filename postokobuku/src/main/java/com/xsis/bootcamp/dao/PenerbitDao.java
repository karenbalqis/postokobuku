package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PenerbitModel;

public interface PenerbitDao {
	public List<PenerbitModel> get() throws Exception;
	public List<PenerbitModel> getByPropinsiId (int id) throws Exception;
	public List<PenerbitModel> getByKotaId (int id) throws Exception;
	public List<PenerbitModel> getByKecamatanId (int id) throws Exception;
	public void insert(PenerbitModel model) throws Exception;
	public PenerbitModel getById(int id) throws Exception;
	public void update(PenerbitModel model) throws Exception;
	public void delete(PenerbitModel model) throws Exception;
}
