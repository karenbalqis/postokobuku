package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.UserDao;
import com.xsis.bootcamp.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<UserModel> get() throws Exception {		
		Session session = this.sessionFactory.getCurrentSession();
		List<UserModel> result = session.createQuery("from UserModel").list();
		return result;
	}

	@Override
	public void insert(UserModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public UserModel getById(Integer id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		UserModel result = session.get(UserModel.class, id);
		return result;
	}

	@Override
	public void update(UserModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(UserModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
