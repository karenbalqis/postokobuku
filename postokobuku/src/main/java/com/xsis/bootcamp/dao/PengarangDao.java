package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PengarangModel;

public interface PengarangDao {
	public List<PengarangModel> get() throws Exception;
	
	public void insert(PengarangModel model) throws Exception;
	public PengarangModel getById(Integer id) throws Exception;
	public void update(PengarangModel model) throws Exception;
	public void delete(PengarangModel model) throws Exception;
}
