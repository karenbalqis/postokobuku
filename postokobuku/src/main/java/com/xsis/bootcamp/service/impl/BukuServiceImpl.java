package com.xsis.bootcamp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.BukuDao;
import com.xsis.bootcamp.dao.PenerbitDao;
import com.xsis.bootcamp.dao.StokDao;
import com.xsis.bootcamp.model.BukuModel;
import com.xsis.bootcamp.model.StokModel;
import com.xsis.bootcamp.service.BukuService;

@Service
@Transactional
public class BukuServiceImpl implements BukuService {
	@Autowired
	private BukuDao dao;
	
	@Autowired
	private StokDao stokDao;
	
	@Autowired
	private PenerbitDao penerbitDao;
	
	

	@Override
	public List<BukuModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public BukuModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void insert(BukuModel model) throws Exception {
		//Mendapatkan kota dimana buku diterbitkan berdasarkan penerbit
		Integer idKotaPenerbit = penerbitDao.getById(model.getPenerbitId()).getKotaId();
		
		//Set kota pada model buku yang akan disimpan
		model.setKotaId(idKotaPenerbit);
		
		//Simpan buku
		this.dao.insert(model);
				
		//Proses Inisisai properties baru ke tabel stok
		StokModel stokModel = new StokModel();		
		stokModel.setJumlahStok(0); //set jumlah buku = 0
		stokModel.setBukuId(model.getId()); //set id dari model buku
		stokModel.setBukuModel(model); //set model
		
		//Set tanggal
		Date date = new Date();
		stokModel.setTanggal(date);
		
		//Proses insert ke table stok
		stokDao.insert(stokModel);		
	}

	@Override
	public void update(BukuModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(BukuModel model) throws Exception {
		this.dao.delete(model);
	}

}
