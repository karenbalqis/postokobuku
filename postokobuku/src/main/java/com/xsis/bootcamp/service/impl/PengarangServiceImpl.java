package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PengarangDao;
import com.xsis.bootcamp.model.PengarangModel;
import com.xsis.bootcamp.service.PengarangService;

@Service
@Transactional
public class PengarangServiceImpl implements PengarangService {
	@Autowired
	private PengarangDao dao;
	
	@Override
	public List<PengarangModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public PengarangModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void insert(PengarangModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public void update(PengarangModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(PengarangModel model) throws Exception {
		this.dao.delete(model);
	}

}
