package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.UserModel;

public interface UserService {
	//Read (Select)
	public List<UserModel> get() throws Exception;
	public UserModel getById(Integer id) throws Exception;
	public List<UserModel> getByNama(String nama) throws Exception;
	public List<UserModel> getByRoleId(Integer roleId) throws Exception;
	
	//Create (Insert)
	public void insert(UserModel model) throws Exception;
	
	//Update
	public void update(UserModel model) throws Exception;
	
	//Delete
	public void delete(UserModel model) throws Exception;
}
