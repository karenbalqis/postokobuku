package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.SupplierModel;

public interface SupplierService {
	//Read (select)
	public List<SupplierModel> get() throws Exception;
	public SupplierModel getById(Integer id) throws Exception;
	
	//Create (Insert)
	public void insert(SupplierModel model) throws Exception;
	
	//Update
	public void update(SupplierModel model) throws Exception;
	
	//Delete
	public void delete(SupplierModel model) throws Exception;
}
