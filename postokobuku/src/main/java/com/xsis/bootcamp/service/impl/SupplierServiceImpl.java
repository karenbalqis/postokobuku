package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.SupplierDao;
import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.service.SupplierService;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {
	@Autowired
	private SupplierDao dao;
	
	@Override
	public List<SupplierModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public SupplierModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void insert(SupplierModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public void update(SupplierModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(SupplierModel model) throws Exception {
		this.dao.delete(model);
	}

}
