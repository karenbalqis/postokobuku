package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PenerbitDao;
import com.xsis.bootcamp.model.PenerbitModel;
import com.xsis.bootcamp.service.PenerbitService;

@Service
@Transactional
public class PenerbitServiceImpl implements PenerbitService {
	
	@Autowired
	private PenerbitDao dao;
	
	@Override
	public List<PenerbitModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(PenerbitModel model) throws Exception {
		this.dao.insert(model);

	}

	@Override
	public PenerbitModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(PenerbitModel model) throws Exception {
		this.dao.update(model);

	}

	@Override
	public void delete(PenerbitModel model) throws Exception {
		this.dao.delete(model);

	}

	

}
