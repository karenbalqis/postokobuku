package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.BukuDao;
import com.xsis.bootcamp.dao.PenjualanDao;
import com.xsis.bootcamp.dao.PenjualanDetailDao;
import com.xsis.bootcamp.dao.StokDao;
import com.xsis.bootcamp.dao.StokHistoriDao;
import com.xsis.bootcamp.model.BukuModel;
import com.xsis.bootcamp.model.PenjualanDetailModel;
import com.xsis.bootcamp.model.PenjualanModel;
import com.xsis.bootcamp.model.StokHistoriModel;
import com.xsis.bootcamp.model.StokModel;
import com.xsis.bootcamp.service.PenjualanService;

@Service
@Transactional
public class PenjualanServiceImpl implements PenjualanService {
	@Autowired
	private PenjualanDao dao;

	@Autowired
	private StokDao stokDao;

	@Autowired
	private StokHistoriDao stokHistoriDao;

	@Autowired
	private BukuDao bukuDao;

	@Autowired
	private PenjualanDetailDao penjualanDetailDao;

	@Override
	public List<PenjualanModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public PenjualanModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void insert(PenjualanModel model) throws Exception {
		// Menghitung total penjualan
		Double total = new Double(0);
		BukuModel bukuModel;

		this.dao.insert(model);
		for (PenjualanDetailModel detail : model.getPenjualanDetailModels()) {
			PenjualanDetailModel item = new PenjualanDetailModel();
			item.setPenjualanId(model.getId());
			item.setBukuId(detail.getBukuId());
			item.setHarga(detail.getHarga());
			item.setJumlah(detail.getJumlah());
			item.setSubTotal(detail.getSubTotal());

			bukuModel = this.bukuDao.getById(item.getBukuId());

			/* =================== Menghitung Total ==================== */
			total = total + bukuModel.getHargaJual();

			/* =================== Update Stok Buku ==================== */
			StokModel stokModel = bukuModel.getStokModel();

			Integer stokAwal = stokModel.getJumlahStok();
			Integer stokKurang = detail.getJumlah();
			Integer stokAkhir = stokAwal - stokKurang;
			stokModel.setJumlahStok(stokAkhir);

			/* =================== Kurang Stok Histori ================= */
			StokHistoriModel stokHistoriModel = new StokHistoriModel();
			stokHistoriModel.setBukuId(bukuModel.getId());
			stokHistoriModel.setJumlahJual(0);
			stokHistoriModel.setJumlahJual(stokKurang);
			stokHistoriModel.setStokAkhir(stokAkhir);

			/* =================== Eksekusi ========================== */
			this.penjualanDetailDao.insert(item);
			this.stokDao.update(stokModel);
			this.stokHistoriDao.insert(stokHistoriModel);
		}

	}

	@Override
	public void update(PenjualanModel model) throws Exception {
		this.dao.update(model);

	}

	@Override
	public void delete(PenjualanModel model) throws Exception {
		this.dao.delete(model);

	}

}
