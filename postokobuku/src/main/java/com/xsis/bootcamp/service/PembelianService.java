package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PembelianModel;

public interface PembelianService {
	//Read (select)
	public List<PembelianModel> get() throws Exception;
	public PembelianModel getById(Integer id) throws Exception;
	
	//Create (Insert)
	public void insert(PembelianModel model) throws Exception;
	
	//Update
	public void update(PembelianModel model) throws Exception;
	
	//Delete
	public void delete(PembelianModel model) throws Exception;
}
