package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PenjualanModel;

public interface PenjualanService {
	public List<PenjualanModel> get() throws Exception;
	public PenjualanModel getById(Integer id) throws Exception;
	
	public void insert(PenjualanModel model) throws Exception;
	
	public void update(PenjualanModel model) throws Exception;
	
	public void delete(PenjualanModel model) throws Exception;
}

