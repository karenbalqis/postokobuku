package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.BukuDao;
import com.xsis.bootcamp.dao.PembelianDao;
import com.xsis.bootcamp.dao.PembelianDetailDao;
import com.xsis.bootcamp.dao.StokDao;
import com.xsis.bootcamp.dao.StokHistoriDao;
import com.xsis.bootcamp.model.BukuModel;
import com.xsis.bootcamp.model.PembelianDetailModel;
import com.xsis.bootcamp.model.PembelianModel;
import com.xsis.bootcamp.model.StokHistoriModel;
import com.xsis.bootcamp.model.StokModel;
import com.xsis.bootcamp.service.PembelianService;

@Service
@Transactional
public class PembelianServiceImp implements PembelianService {
	@Autowired
	private PembelianDao dao;
	
	@Autowired
	private PembelianDetailDao pembelianDetailDao;
	
	@Autowired
	private StokDao stokDao;
	
	@Autowired
	private StokHistoriDao stokHistoriDao;
	
	@Autowired
	private BukuDao bukuDao;
	
		
	@Override
	public List<PembelianModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public PembelianModel getById(Integer id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void insert(PembelianModel model) throws Exception {
		//Menghitung total pembelian
		Double subTotal = (double) 0;
		Double grandTotal = (double) 0;
		
		BukuModel bukuModel;
		
		this.dao.insert(model);
		for (PembelianDetailModel detail : model.getDetailPembelians()) {
			//Memasukkan buku yang dibeli ke detail pembelian			
			PembelianDetailModel item = new PembelianDetailModel();
			
				
						
			bukuModel = this.bukuDao.getById(detail.getBukuId());
			item.setPembelianId(model.getId());
			item.setBukuId(bukuModel.getId());
			item.setHarga(bukuModel.getHargaBeli());
			item.setJumlah(detail.getJumlah());
				
			
			
			/*=================== Menghitung Total ====================*/
			subTotal = detail.getJumlah()*bukuModel.getHargaBeli();
			item.setSubTotal(subTotal);
			
			grandTotal += subTotal;
			
			/*=================== Update Stok Buku ====================*/
			StokModel stokModel = bukuModel.getStokModel();
			
			Integer stokAwal 	= stokModel.getJumlahStok();
			Integer stokTambah 	= detail.getJumlah();
			Integer stokAkhir 	= stokAwal+stokTambah;			
			stokModel.setJumlahStok(stokAkhir);
			
			/*=================== Tambah Stok Histori =================*/
			StokHistoriModel stokHistoriModel = new StokHistoriModel();
			stokHistoriModel.setBukuId(bukuModel.getId());
			stokHistoriModel.setJumlahJual(0);
			stokHistoriModel.setJumlahBeli(stokTambah);
			stokHistoriModel.setStokAkhir(stokAkhir);
			
			/*=================== 	Eksekusi  ==========================*/
			this.pembelianDetailDao.insert(item);
			this.stokDao.update(stokModel);
			this.stokHistoriDao.insert(stokHistoriModel);
		}		
		
		//Mengisi total harga
		model.setTotal(grandTotal);
		//Masukkan total harga
		this.dao.update(model);
	}

	@Override
	public void update(PembelianModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(PembelianModel model) throws Exception {
		this.dao.delete(model);
	}

}
