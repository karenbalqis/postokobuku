package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.KategoriDao;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.service.KategoriService;

@Service
@Transactional
public class KategoriServiceImpl implements KategoriService {
	@Autowired
	private KategoriDao dao;
	
	@Override
	public List<KategoriModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public void insert(KategoriModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public KategoriModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void update(KategoriModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(KategoriModel model) throws Exception {
		this.dao.delete(model);		
	}

}
