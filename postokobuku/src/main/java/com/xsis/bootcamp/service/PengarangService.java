package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PengarangModel;

public interface PengarangService {
	//Read (select)
	public List<PengarangModel> get() throws Exception;
	public PengarangModel getById(Integer id) throws Exception;
	
	//Create (Insert)
	public void insert(PengarangModel model) throws Exception;
	
	//Update
	public void update(PengarangModel model) throws Exception;
	
	//Delete
	public void delete(PengarangModel model) throws Exception;
}
