package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PelangganModel;

public interface PelangganService {
	//Read (select)
	public List<PelangganModel> get() throws Exception;
	public PelangganModel getById(Integer id) throws Exception;
	
	//Create (Insert)
	public void insert(PelangganModel model) throws Exception;
	
	//Update
	public void update(PelangganModel model) throws Exception;
	
	//Delete
	public void delete(PelangganModel model) throws Exception;
}
