package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PenerbitModel;

public interface PenerbitService {
	public List<PenerbitModel> get() throws Exception;
	
	public void insert(PenerbitModel model) throws Exception;
	public PenerbitModel getById(int id) throws Exception;
	public void update(PenerbitModel model) throws Exception;
	public void delete(PenerbitModel model) throws Exception;

}
