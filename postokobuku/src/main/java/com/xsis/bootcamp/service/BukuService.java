package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.BukuModel;

public interface BukuService {
	//Read (select)
	public List<BukuModel> get() throws Exception;
	public BukuModel getById(Integer id) throws Exception;
	
	//Create (Insert)
	public void insert(BukuModel model) throws Exception;
	
	//Update
	public void update(BukuModel model) throws Exception;
	
	//Delete
	public void delete(BukuModel model) throws Exception;
}
