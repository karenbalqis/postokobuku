package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.RoleModel;

public interface RoleService {
	//Read (Select)
	public List<RoleModel> get() throws Exception;
	public RoleModel getById(Integer id) throws Exception;
	public List<RoleModel> getByNama(String nama) throws Exception;
	
	
	//Create (Insert)
	public void insert(RoleModel model) throws Exception;
	
	//Update
	public void update(RoleModel model) throws Exception;
	
	//Delete
	public void delete(RoleModel model) throws Exception;
}
