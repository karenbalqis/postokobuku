package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.PenerbitModel;
import com.xsis.bootcamp.service.PenerbitService;

@Controller
public class PenerbitController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PenerbitService penerbitService;

	@RequestMapping(value = "/penerbit")
	public String index(){
		return "penerbit";		
	}
	
	@RequestMapping(value="/penerbit/list")
	public String list(Model model){
		List<PenerbitModel> items = null;
		
		try{
			items = this.penerbitService.get();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		model.addAttribute("list", items);
		
		return "penerbit/list";
	}
	
	@RequestMapping(value="penerbit/add")
	public String add(){
		return "penerbit/add";
	}
	
	@RequestMapping (value="penerbit/delete")
	public String delete(Model model, HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		PenerbitModel penerbitModel = null;
		
		try{
			penerbitModel = this.penerbitService.getById(id);
		}
		catch(Exception e){
			log.error("Error @ tarik data penerbit @delete : " + e.getMessage() ,e);
		}
		model.addAttribute("item", penerbitModel);
		return "penerbit/delete";
	}
	
	@RequestMapping(value ="penerbit/edit")
	public String edit (Model model, HttpServletRequest request ){
		Integer id = Integer.parseInt(request.getParameter("id"));
		PenerbitModel penerbitModel = null;
		
		try {
			penerbitModel = this.penerbitService.getById(id);
		} catch (Exception e){
			log.error("Error @ tarik data edit" + e.getMessage(), e);
		}
		
		model.addAttribute("item", penerbitModel);
		return "penerbit/edit";
	}
	
	@RequestMapping(value="/penerbit/save")
	private String save(Model model, @ModelAttribute PenerbitModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			if (proses.equals("insert")){
				this.penerbitService.insert(item);
			} else if(proses.equals("update")){
				this.penerbitService.update(item);
			} else if(proses.equals("delete")){
				this.penerbitService.delete(item);
			}
			result = "berhasil";
		}
		catch (Exception e){
			log.error("Error @save penerbit :" +e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result", result);
		return "penerbit/save";
	}
}
