package com.xsis.bootcamp.controller;
/*
 * Testing
 */

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.BukuModel;
import com.xsis.bootcamp.service.BukuService;

@Controller
public class BukuController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private BukuService bukuService;
	
	@RequestMapping(value = "/buku")
	public String index() {		
		return "buku";
	}

	@RequestMapping(value = "/buku/list")
	public String list(Model model) {
		List<BukuModel> items = null;

		try {
			items = this.bukuService.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "buku/list";
	}

	@RequestMapping(value = "/buku/add")
	public String add() {
		return "buku/add";
	}
	
	@RequestMapping(value = "/buku/delete")
	public String delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		BukuModel buku = null;
		
		try {
			buku = this.bukuService.getById(id);
		} catch (Exception e) {
			log.error("Error tarik data buku @delete: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", buku);
		return "buku/delete";
	}
	
	@RequestMapping(value = "/buku/edit")
	public String edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		BukuModel bukuModel = null;
		
		try {
			bukuModel = this.bukuService.getById(id);
		} catch (Exception e) {
			log.error("Error @ tarik data edit: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", bukuModel);
		return "buku/edit";
	}

	@RequestMapping(value = "/buku/save")
	private String save(Model model, @ModelAttribute BukuModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			
			if (proses.equals("insert")) {
				this.bukuService.insert(item);
			} else if (proses.equals("update")) {
				this.bukuService.update(item);
			} else if (proses.equals("delete")) {
				this.bukuService.delete(item);
			}
			result = "berhasil";			
		} catch (Exception e) {
			log.error("Error @ save buku: "+e.getMessage(), e);
			result = "gagal";
		}
		
		model.addAttribute("result", result);
		return "buku/save";
	}
}
