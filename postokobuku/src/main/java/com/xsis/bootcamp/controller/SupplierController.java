package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.service.SupplierService;

@Controller
public class SupplierController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private SupplierService service;

	@RequestMapping(value = "/supplier")
	public String index() {
		return "supplier";
	}

	@RequestMapping(value = "/supplier/list")
	public String list(Model model) {
		List<SupplierModel> items = null;

		try {
			items = this.service.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "supplier/list";
	}

	@RequestMapping(value = "/supplier/add")
	public String add() {
		return "supplier/add";
	}
	
	@RequestMapping(value = "/supplier/delete")
	public String delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		SupplierModel supplier = null;
		
		try {
			supplier = this.service.getById(id);
		} catch (Exception e) {
			log.error("Error tarik data supplier @delete: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", supplier);
		return "supplier/delete";
	}
	
	@RequestMapping(value = "/supplier/edit")
	public String edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		SupplierModel supplierModel = null;
		
		try {
			supplierModel = this.service.getById(id);
		} catch (Exception e) {
			log.error("Error @ tarik data edit: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", supplierModel);
		return "supplier/edit";
	}

	@RequestMapping(value = "/supplier/save")
	private String save(Model model, @ModelAttribute SupplierModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else if (proses.equals("delete")) {
				this.service.delete(item);
			}
			result = "berhasil";			
		} catch (Exception e) {
			log.error("Error @ save supplier: "+e.getMessage(), e);
			result = "gagal";
		}
		
		model.addAttribute("result", result);
		return "supplier/save";
	}
}
