package com.xsis.bootcamp.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.RoleModel;
import com.xsis.bootcamp.service.RoleService;

@Controller
public class RoleController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private RoleService service;
	
	@RequestMapping(value="/role")
	public String index(){
		return "role";
	}
	
	@RequestMapping(value="/role/list")
	public String list(Model model){
		List<RoleModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error("Ambil list role error: "+e.getMessage(), e);
		}
		
		model.addAttribute("list", items);		
		return "role/list";
	}
	
	@RequestMapping(value="/role/add")
	private String add() {		
		return "role/add";
	}
	
	@RequestMapping(value="/role/delete")
	private String delete(HttpServletRequest request, Model model){
		//Tangkap parameter ('id') yang dilempar browser
		int id = Integer.parseInt(request.getParameter("id"));
		
		//Buat objek kosong
		RoleModel roleModel = null;
		
		
		//Isi objek kosong tersebut
		try {
			roleModel = service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		//Lempar objek sebelumnya ke view
		model.addAttribute("item", roleModel);
		
		return "role/delete";
	}
	
	@RequestMapping(value="/role/edit")
	private String edit(Model model, HttpServletRequest request) {
		//Tangkap id
		int id = Integer.parseInt(request.getParameter("id"));
		
		//Siapkan objek
		RoleModel roleModel = null;
		
		//Isi objek
		try {
			roleModel = service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("item", roleModel);
		return "role/edit";
	}
	
	
	/*Handling Perubahan (insert, update, delete)*/
	@RequestMapping(value="/role/save")
	private String save(Model model, @ModelAttribute RoleModel item, HttpServletRequest request){ 
		//Ambil parameter penentu
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")){
				this.service.update(item);
			} else if (proses.equals("delete")){
				this.service.delete(item);
			}
			result = "berhasil";			
		} catch (Exception e) {
			log.error("Error proses save: "+e.getMessage(), e);
			result = "gagal";
		}
		
		model.addAttribute("result", result);
		return "role";
	}
}
