package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.PengarangModel;
import com.xsis.bootcamp.service.PengarangService;

@Controller
public class PengarangController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PengarangService service;

	@RequestMapping(value = "/pengarang")
	public String index() {
		return "pengarang";
	}

	@RequestMapping(value = "/pengarang/list")
	public String list(Model model) {
		List<PengarangModel> items = null;

		try {
			items = this.service.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "pengarang/list";
	}

	@RequestMapping(value = "/pengarang/add")
	public String add() {
		return "pengarang/add";
	}
	
	@RequestMapping(value = "/pengarang/delete")
	public String delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		PengarangModel pengarang = null;
		
		try {
			pengarang = this.service.getById(id);
		} catch (Exception e) {
			log.error("Error tarik data pengarang @delete: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", pengarang);
		return "pengarang/delete";
	}
	
	@RequestMapping(value = "/pengarang/edit")
	public String edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		PengarangModel pengarangModel = null;
		
		try {
			pengarangModel = this.service.getById(id);
		} catch (Exception e) {
			log.error("Error @ tarik data edit: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", pengarangModel);
		return "pengarang/edit";
	}

	@RequestMapping(value = "/pengarang/save")
	private String save(Model model, @ModelAttribute PengarangModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else if (proses.equals("delete")) {
				this.service.delete(item);
			}
			result = "berhasil";			
		} catch (Exception e) {
			log.error("Error @ save pengarang: "+e.getMessage(), e);
			result = "gagal";
		}
		
		model.addAttribute("result", result);
		return "pengarang/save";
	}
}
