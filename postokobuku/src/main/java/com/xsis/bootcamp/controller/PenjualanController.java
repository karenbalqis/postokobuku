package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.bootcamp.model.BukuModel;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.PenjualanDetailModel;
import com.xsis.bootcamp.model.PenjualanModel;
import com.xsis.bootcamp.service.BukuService;
import com.xsis.bootcamp.service.PelangganService;
import com.xsis.bootcamp.service.PenjualanService;

@Controller
@RequestMapping(value="/penjualan")
public class PenjualanController {
	@Autowired
	private PenjualanService penjualanService;
	
	@Autowired
	private PelangganService pelangganService;
	
	@Autowired
	private BukuService bukuService;
	
	Log log = LogFactory.getLog(getClass());
	
	@RequestMapping
	public String Home(Model model){
		return "penjualan";
	}
	
	@RequestMapping(value="/list")
	public String list(Model model){
		List<PenjualanModel> item = null;
		
		try {
			item = this.penjualanService.get();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		model.addAttribute("list", item);
		return "/penjualan/list";
	}
	
	@RequestMapping(value="/add")
	public String add(){
		return "penjualan/add";
	}
	
	@RequestMapping(value="/listPelanggan")
	public String listPelanggan(Model model){
		List<PelangganModel> item = null;
		try {
			item = this.pelangganService.get();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		model.addAttribute("list", item);
		return "/penjualan/listPelanggan";
	}
	
	@RequestMapping(value = "/listBuku")
	public String listBuku(Model model){
		List<BukuModel> item = null;
		try{
			item = this.bukuService.get();
		} catch (Exception e){
			e.printStackTrace();
		}
		model.addAttribute("list", item);
		return "penjualan/listBuku";
	}
	
	@RequestMapping(value = "/delete")
	public String delete(Model model, HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		PenjualanModel penjualan = null;
		
		try{
			penjualan = this.penjualanService.getById(id);
		} catch (Exception e){
			log.error("Error tarik data penjualan @delete :" + e.getMessage(), e);
		}
		model.addAttribute("item", penjualan);
		return "penjualan/delete";
	}
	
	@RequestMapping(value="penjualan/edit")
	public String edit(Model model, HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		PenjualanModel penjualanModel = null;
		
		try{
			penjualanModel = this.penjualanService.getById(id);
		} catch (Exception e){
			log.error("Error @ tarik data edit" + e.getMessage(), e);
		}
		model.addAttribute("list", penjualanModel);
		return "/penjualan/edit";
	}
	
	@RequestMapping(value="/detailPenjualan")
	public String penjualanDetail(Model model, HttpServletRequest request){
		PenjualanModel penjualanModel = new PenjualanModel();
		List<PenjualanDetailModel> items = null;
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		try{
			penjualanModel = penjualanService.getById(id);
			items = penjualanModel.getPenjualanDetailModels();
		} catch (Exception e){
			log.error("Error @ Load Detail Penjualan :"+ e.getMessage(), e );
		}
		
		
		model.addAttribute("list" , items);
		model.addAttribute("item", penjualanModel);
		System.out.println("panjang list: "+items.size());
		return "penjualan/listDetailPenjualan";
	}
	
	@RequestMapping(value ="/save", method= RequestMethod.POST)
	private String save (Model model, @ModelAttribute PenjualanModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		
		System.out.println("Testing: "+item.getPenjualanDetailModels());
		
		try{
			if (proses.equals("insert")){
				this.penjualanService.insert(item);
			} else if(proses.equals("update")){
				this.penjualanService.update(item);
			} else if(proses.equals("delete")){
				this.penjualanService.delete(item);
			}
			result ="berhasil";
		} catch (Exception e){
			log.error("Error @ save penjualan" +e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result", result);
		return "penjualan/service";
	}
	
	@RequestMapping(value="/penjualan/bukuSearch")
	public String pasienSearch(Model model, HttpServletRequest request){
		// membuat object list dari class Mahasiswa model
				List<BukuModel> items = null;
				
				try {
					// object items diisi data dari method get
					items = this.bukuService.get();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				
				// datanya kita kirim ke view, 
				// kita buat variable list kemudian diisi dengan object items
				model.addAttribute("list", items);
				
		return "penjualan/listbarang";
	}
	
	
	
}
