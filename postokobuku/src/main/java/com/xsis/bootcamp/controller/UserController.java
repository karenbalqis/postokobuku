package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.RoleModel;
import com.xsis.bootcamp.model.UserModel;
import com.xsis.bootcamp.service.RoleService;
import com.xsis.bootcamp.service.UserService;

@Controller
public class UserController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private UserService service;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value="/user")
	public String index() {
		return "user";
	}
	
	@RequestMapping(value="/user/list")
	public String list(Model model){
		List<UserModel> items = null;
		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		
		
		model.addAttribute("list", items);
		return "user/list";
	}
	
	@RequestMapping(value="/user/add")
	public String add(Model model){
		//Mendeklarasikan items untuk diisi dengan objek role
		List<RoleModel> listRole = null;		
		
		try {			
			listRole = roleService.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("list", listRole);
		return "user/add";
	}
	
	@RequestMapping(value="/user/save")
	public String save(Model model, @ModelAttribute UserModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")){
				this.service.update(item);
			} else if (proses.equals("delete")){
				this.service.delete(item);
			}
			
			result = "berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "gagal";
		}
		
		model.addAttribute("result", result);
		return "user/save";
	}
	
	@RequestMapping(value="/user/delete")
	public String delete(Model model, HttpServletRequest request){
		//Tangkap parameter yang dikirimkan client (berupa id)
		int id = Integer.parseInt(request.getParameter("id"));
		
		//Siapkan objek untuk di 'set' sebagai user
		UserModel user = null;
		
		//Coba cari user pada database berdasarkan id yang didapat
		try {
			user = this.service.getById(id);
		} catch (Exception e) {
			//Kalau gagal, throw exception
			log.error(e.getMessage(), e);
		}
		
		//Operkan objek user sebagai item di pop up konfirmasi hapus
		model.addAttribute("item", user);
		return "user/delete";
	}
	
	@RequestMapping(value="/user/edit")
	public String edit(Model model, HttpServletRequest request){
		//Tangkap id yang dikirim client (konversikan dari bentuk string ke integer pula)
		int id = Integer.parseInt(request.getParameter("id"));
		
		//Siapkan objek kosong untuk di 'set' sebagai user berdasarkan id yang didapat
		UserModel user = null;
		
		//Coba cari user di database berdasarkan id yang dimiliki kemudian simpan propertisnya pada user
		try {
			user = this.service.getById(id);
		} catch (Exception e) {
			//Jika gagal, kirim log
			log.error(e.getMessage(), e);
		}
		
		//Siapkan atribut untuk mengisi role
		List<RoleModel> roleModels = null;
		
		//Coba cari list role di database
		try {
			roleModels = roleService.get();
		} catch (Exception e) {
			log.error("Error tarik data list role: "+e.getMessage(), e);
		}
		
		//Lemparkan user ke view dengan nama atribut 'item'
		model.addAttribute("item", user);
		//Lemparkan list roleModels ke view dengan nama atribut 'list'
		model.addAttribute("list", roleModels);
		
		return "user/edit";
	}
}
