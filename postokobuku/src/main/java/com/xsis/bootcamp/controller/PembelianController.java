package com.xsis.bootcamp.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.bootcamp.model.BukuModel;
import com.xsis.bootcamp.model.PembelianDetailModel;
import com.xsis.bootcamp.model.PembelianModel;
import com.xsis.bootcamp.model.SupplierModel;
import com.xsis.bootcamp.service.BukuService;
import com.xsis.bootcamp.service.PembelianService;
import com.xsis.bootcamp.service.SupplierService;

@Controller
@RequestMapping(value = "/pembelian")
public class PembelianController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PembelianService pembelianService;

	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private BukuService bukuService;
	
	

	@RequestMapping
	public String index() {
		return "pembelian";
	}

	@RequestMapping(value = "/list")
	public String list(Model model) {
		List<PembelianModel> items = null;

		try {
			items = this.pembelianService.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "pembelian/list";
	}

	@RequestMapping(value = "/add")
	public String add() {
		return "pembelian/add";
	}

	@RequestMapping(value = "/listSupplier")
	public String listSupplier(Model model) {
		List<SupplierModel> items = null;

		try {
			items = this.supplierService.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "pembelian/listSupplier";
	}

	@RequestMapping(value = "/listBuku")
	public String listBuku(Model model) {
		List<BukuModel> items = null;

		try {
			items = this.bukuService.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "pembelian/listBuku";
	}
	
	@RequestMapping(value = "/detailPembelian")
	public String pembelianDetail(Model model, HttpServletRequest request){
		PembelianModel pembelianModel = new PembelianModel();
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		try {
			pembelianModel = pembelianService.getById(id);
		} catch (Exception e) {
			log.error("Error @ Load Detail Pembelian: "+e.getMessage(), e);
		}
		
		List<PembelianDetailModel> items = pembelianModel.getDetailPembelians();
		
		model.addAttribute("list", items);
		model.addAttribute("item", pembelianModel);
		return "pembelian/listDetailPembelian";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	private String save(Model model, @ModelAttribute PembelianModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		
		System.out.println("Detail Pembelian: "+item.getDetailPembelians().toString());

		try {

			if (proses.equals("insert")) {
				this.pembelianService.insert(item);
			} else if (proses.equals("update")) {
				this.pembelianService.update(item);
			} else if (proses.equals("delete")) {
				this.pembelianService.delete(item);
			}
			result = "berhasil";
		} catch (Exception e) {
			log.error("Error @ save pembelian: " + e.getMessage(), e);
			result = "gagal";
		}

		model.addAttribute("result", result);
		return "pembelian/save";
	}
}
