package com.xsis.bootcamp.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.model.KecamatanModel;
import com.xsis.bootcamp.model.KotaModel;
import com.xsis.bootcamp.model.PenerbitModel;
import com.xsis.bootcamp.model.PengarangModel;
import com.xsis.bootcamp.model.ProvinsiModel;
import com.xsis.bootcamp.service.KategoriService;
import com.xsis.bootcamp.service.KecamatanService;
import com.xsis.bootcamp.service.KotaService;
import com.xsis.bootcamp.service.PenerbitService;
import com.xsis.bootcamp.service.PengarangService;
import com.xsis.bootcamp.service.ProvinsiService;

@Controller
public class AjaxController {
	private Log log = LogFactory.getLog(this.getClass());

	@Autowired
	private ProvinsiService provinsiService;

	@Autowired
	private KotaService kotaService;

	@Autowired
	private KecamatanService kecamatanService;
	
	@Autowired
	private PengarangService pengarangService;
	
	@Autowired
	private KategoriService kategoriService;
	
	@Autowired
	private PenerbitService penerbitService;
	
	@RequestMapping(value="/ajax/getProvinsi")
	public String getProvinsi(Model model) {
		List<ProvinsiModel> provinsi = null;

		try {
			provinsi = provinsiService.get();
		} catch (Exception e) {
			log.error("Galat memuat provinsi: " + e.getMessage(), e);
		}
		
		model.addAttribute("result", provinsi);
		return "ajax/getProvinsi";
	}
	
	@RequestMapping(value="/ajax/getKotaByProvinsiId")
	public String getKota(Model model, HttpServletRequest request) {
		List<KotaModel> kota = null;
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		try {
			kota = kotaService.getByProvinsiId(id);
		} catch (Exception e) {
			log.error("Galat memuat kota: " + e.getMessage(), e);
		}
		
		model.addAttribute("result", kota);
		return "ajax/getKota";
	}
	
	@RequestMapping(value="/ajax/getKecamatanByKotaId")
	public String getKecamatan(Model model, HttpServletRequest request) {
		List<KecamatanModel> kecamatan = null;
		Integer id = Integer.parseInt(request.getParameter("id"));
		
		try {
			kecamatan = kecamatanService.getByKotaId(id);
		} catch (Exception e) {
			log.error("Galat memuat kecamatan: "+e.getMessage(), e);
		}
		
		model.addAttribute("result", kecamatan);
		return "ajax/getKecamatan";
	}
	
	@RequestMapping(value="/ajax/getPengarang")
	public String getPengarang(Model model){
		List<PengarangModel> pengarang = null;
		
		try {
			pengarang = pengarangService.get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("result", pengarang);
		return "ajax/getPengarang";
	}
	
	@RequestMapping(value="/ajax/getKategori")
	public String getKategori(Model model){
		List<KategoriModel> kategori = null;
		
		try {
			kategori = kategoriService.get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("result", kategori);
		return "ajax/getKategori";
	}
	
	@RequestMapping(value="/ajax/getPenerbit")
	public String getPenerbit(Model model){
		List<PenerbitModel> penerbit = null;
		
		try {
			penerbit = penerbitService.get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("result", penerbit);
		return "ajax/getPenerbit";
	}
}
