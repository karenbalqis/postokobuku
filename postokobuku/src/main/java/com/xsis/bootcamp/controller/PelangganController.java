package com.xsis.bootcamp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.service.PelangganService;

@Controller
public class PelangganController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PelangganService service;

	@RequestMapping(value = "/pelanggan")
	public String index() {
		return "pelanggan";
	}

	@RequestMapping(value = "/pelanggan/list")
	public String list(Model model) {
		List<PelangganModel> items = null;

		try {
			items = this.service.get();
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("list", items);
		return "pelanggan/list";
	}

	@RequestMapping(value = "/pelanggan/add")
	public String add() {
		return "pelanggan/add";
	}
	
	@RequestMapping(value = "/pelanggan/delete")
	public String delete(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		PelangganModel pelanggan = null;
		
		try {
			pelanggan = this.service.getById(id);
		} catch (Exception e) {
			log.error("Error tarik data pelanggan @delete: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", pelanggan);
		return "pelanggan/delete";
	}
	
	@RequestMapping(value = "/pelanggan/edit")
	public String edit(Model model, HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id"));
		PelangganModel pelangganModel = null;
		
		try {
			pelangganModel = this.service.getById(id);
		} catch (Exception e) {
			log.error("Error @ tarik data edit: "+e.getMessage(), e);
		}
		
		model.addAttribute("item", pelangganModel);
		return "pelanggan/edit";
	}

	@RequestMapping(value = "/pelanggan/save")
	private String save(Model model, @ModelAttribute PelangganModel item, HttpServletRequest request) {
		String proses = request.getParameter("proses");
		String result = "";
		
		try {
			
			if (proses.equals("insert")) {
				this.service.insert(item);
			} else if (proses.equals("update")) {
				this.service.update(item);
			} else if (proses.equals("delete")) {
				this.service.delete(item);
			}
			result = "berhasil";			
		} catch (Exception e) {
			log.error("Error @ save pelanggan: "+e.getMessage(), e);
			result = "gagal";
		}
		
		model.addAttribute("result", result);
		return "pelanggan/save";
	}
}
