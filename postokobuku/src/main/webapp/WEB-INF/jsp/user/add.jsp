<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<form id="form-user" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">Nama User</label>
			<div class="col-md-10">
				<input type="text" id="namaUser" name="namaUser"
					class="form-control" required="required">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Username</label>
			<div class="col-md-10">
				<input type="text" id="username" name="username"
					class="form-control" required="required">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Password</label>
			<div class="col-md-10">
				<input type="text" id="password" name="password"
					class="form-control" required="required">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Role</label>
			<div class="col-md-10">
				<select class="form-control" id="roleId" name="roleId" class="form-control"
					required="required">
					<option value="">Silahkan Pilih</option>
					<c:forEach var="item" items="${list}">
						<option value="${item.id}">${item.namaRole}</option>
					</c:forEach>
				</select>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>