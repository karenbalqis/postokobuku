<form id="form-user" action="update" method="post">
	<div class="form-horizontal">
	<!-- Tarik data dengan variabel item yang dikirim controller, simpan pada form hidden -->
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" value="${item.id}">
		<input type="hidden" id="namaUser" name="namaUser" value="${item.namaUser}">		
		<input type="hidden" id="username" name="username" value="${item.username}">
		<input type="hidden" id="password" name="password" value="${item.password}">
		<input type="hidden" id="roleId" name="roleId" value="${item.roleId}">
		
		<div class="form-group">
			<div class="col-md-12">
				<p>User akses atas nama <b>${item.namaUser}</b> dengan id <b>${item.id}</b> akan dihapus, lanjutakan?</p>
			</div>

		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Lanjutkan Hapus</button>
	</div>
</form>
