<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<form id="form-role" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">Nama Role</label>
			<div class="col-md-10">
				<input type="text" id="namaRole" name="namaRole"
					class="form-control" required="required" maxlength="80">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan</label>
			<div class="col-md-10">
				<input type="text" id="keterangan" name="keterangan"
					class="form-control" required="required" maxlength="100">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>