<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<form id="form-supplier" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">Nama Supplier</label>
			<div class="col-md-10">
				<input type="text" id="namaSupplier" name="namaSupplier"
					class="form-control" required="required" maxlength="60">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Alamat</label>
			<div class="col-md-10">
				<input type="text" id="alamat" name="alamat"
					class="form-control" required="required" maxlength="100">
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label col-md-2">Provinsi</label>
			<div class="col-md-10">
				<select class="form-control" id="provinsiId" name="provinsiId" class="form-control"
					required="required">
					<option value="11">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Kota/Kab.</label>
			<div class="col-md-10">
				<select class="form-control" id="kotaId" name="kotaId" class="form-control"
					required="required">
					<option value="1101">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Kecamatan</label>
			<div class="col-md-10">
				<select class="form-control" id="kecamatanId" name="kecamatanId" class="form-control"
					required="required">
					<option value="1101010">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Telepon</label>
			<div class="col-md-10">
				<input type="text" id="noTelepon" name="noTelepon"
					class="form-control" required="required" maxlength="15">
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Fax</label>
			<div class="col-md-10">
				<input type="text" id="noFax" name="noFax"
					class="form-control" required="required" maxlength="15">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Email</label>
			<div class="col-md-10">
				<input type="email" id="email" name="email"
					class="form-control" required="required" maxlength="60">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>