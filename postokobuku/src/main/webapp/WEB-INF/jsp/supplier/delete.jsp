<form id="form-supplier" action="update" method="post">
	<div class="form-horizontal">
	<!-- Tarik data dengan variabel item yang dikirim controller, simpan pada form hidden -->
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" value="${item.id}">
		<input type="hidden" id="namaSupplier" name="namaSupplier" value="${item.namaSupplier}">		
		<input type="hidden" id="alamat" name="alamat" value="${item.alamat}">
		<input type="hidden" id="provinsiId" name="provinsiId" value="${item.provinsiId}">
		<input type="hidden" id="kecamatanId" name="kecamatanId" value="${item.kecamatanId}">
		<input type="hidden" id="kotaId" name="kotaId" value="${item.kotaId}">
		<input type="hidden" id="kotaId" name="noTelepon" value="${item.noTelepon}">
		<input type="hidden" id="email" name="email" value="${item.email}">
		
		<div class="form-group">
			<div class="col-md-12">
				<p>Supplier atas nama <b>${item.namaSupplier}</b> akan dihapus, lanjutakan?</p>
			</div>

		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Lanjutkan Hapus</button>
	</div>
</form>
