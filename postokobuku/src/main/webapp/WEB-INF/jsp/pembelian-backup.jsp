<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Tabel Pembelian</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> Add Pembelian
			</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Nomor Faktur</th>
					<th>Supplier</th>
					<th>Kategori</th>
					<th>Pengarang</th>
					<th>Tahun</th>
					<th>Penerbit</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog" style="width: 95%; min-height: 95%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal"></h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="konfirmasiTambah"  onclick="addPembelian()">Simpan Pembelian</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-supplier" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Supplier</h4>
				<table class="table">
					<thead>
						<tr>
							<th>Nama Supplier</th>
							<th>No Telepon</th>
							<th>No Faximile</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody id="list-data-supplier">

					</tbody>
				</table>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-buku" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Buku</h4>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>ISBN</th>
							<th>Judul Buku</th>
							<th>Kategori</th>
							<th>Nama Pengarang</th>
							<th>Tahun Terbit</th>
							<th>Harga Jual</th>
							<th>Penerbit</th>
						</tr>
					</thead>
					<tbody id="list-data-buku">

					</tbody>
				</table>
			</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-jumlah-item" class="modal">
	<div class="modal-dialog">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Input Jumlah Beli</h4>
				<div class="modal-body">
					<input class="form-control" id="idBuku" type="hidden"
						readonly="readonly">
					<div class="form-group">
						<label for="judulBuku" class="col-sm-2 control-label">Judul</label>
						<div class="col-sm-10">
							<input class="form-control" id="judulBuku" type="text"
								readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="judulBuku" class="col-sm-2 control-label">Stok</label>
						<div class="col-sm-10">
							<input class="form-control" id="stok" type="text"
								readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="judulBuku" class="col-sm-2 control-label">Harga</label>
						<div class="col-sm-10">
							<input class="form-control" id="hargaBuku" type="text"
								readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="judulBuku" class="col-sm-2 control-label">Jumlah</label>
						<div class="col-sm-10">
							<input class="form-control" id="jumlahBeli" type="text"
								onkeydown="hitungSubTotal()" onkeypress="hitungSubTotal()"
								onkeyup="hitungSubTotal()" autofocus="autofocus">
						</div>
					</div>
					<div class="form-group">
						<label for="judulBuku" class="col-sm-2 control-label">Sub
							Total</label>
						<div class="col-sm-10">
							<input class="form-control" id="subTotal" type="text"
								readonly="readonly">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal" onclick="bersihJumlah()">Batalkan</button>
				<button type="button" class="btn btn-primary" id="konfirmasiTambah">Tambahkan</button>
			</div>
		</div>
	</div>
</div>


<script>
	var itemBeli = [];
	var pembelian = [];

	loadData();
	$("#modal-input").on("click", "#btn-search", function() {
		loadSupplier();
	});

	$("#modal-input").on("click", "#tambah-item", function() {
		loadBuku();
	});

	$("#modal-jumlah-item #konfirmasiTambah").click(function() {
		addItem();
		bersihJumlah();
		showItemBuku();
		$("#modal-jumlah-item").modal('hide');
	});

	function showItemBuku() {
		$("#modal-input #listBeli").empty();
		var tr;
		for (var i = 0; i < itemBeli.length; i++) {
			var obj = itemBeli[i];
			tr = '<tr>'+
					'<td>'+(i+1)+'</td>'+
					'<td>'+obj.bukuId+'</td>'+
					'<td>'+obj.harga+'</td>'+
					'<td>'+obj.jumlah+'</td>'+
					'<td>'+obj.subTotal+'</td>'+
					'<td><button type="button" class="btn btn-block btn-danger btn-xs" '+
					'value="'+i+'" onclick="hapusItemBuku('+i+')">Hapus</button></td>'+
				'</tr>';
				
				
			$("#modal-input #listBeli").append(tr);
		}
	}
	
	function hapusItemBuku(index){
		itemBeli.splice(index, 1);
		showItemBuku();
	}

	function addItem() {
		var newData = {
			bukuId : $("#modal-jumlah-item #idBuku").val(),
			harga : $("#modal-jumlah-item #hargaBuku").val(),
			jumlah : $("#modal-jumlah-item #jumlahBeli").val(),
			subTotal : $("#modal-jumlah-item #subTotal").val(),
		};

		itemBeli.push(newData);
	}
	
	function addPembelian(){
		var newData = {
			nomorFaktur	: "",
			supplierId 	: $("#modal-input #id").val(),
			userId 		: "",
			itemBeli 	: itemBeli
		}
		
		var sourceData = [{"nomorFaktur":"","supplierId":"4","userId":"","detailPembelians":[{"bukuId":"29","harga":"50000.0","jumlah":"4","subTotal":"200000"},{"bukuId":"31","harga":"1000000.0","jumlah":"5","subTotal":"5000000"},{"bukuId":"31","harga":"1000000.0","jumlah":"7","subTotal":"7000000"}]}];
		
		pembelian.push(newData);		
		//console.log(JSON.stringify(sourceData));
		pembelian = [];
		itemBeli = [];
		
		$.ajax({
			url 		: "pembelian/save",
			type		: "POST",
			data		: JSON.stringify(sourceData),
			contentType	: "application/json",
			success		: function(data){
				console.log(data);
			}
		});
		
		$("#modal-input").modal("hide");
	}

	function bersihJumlah() {
		$("#modal-jumlah-item #idBuku").val("");
		$("#modal-jumlah-item #judulBuku").val("");
		$("#modal-jumlah-item #hargaBuku").val("");
		$("#modal-jumlah-item #subTotal").val("");
		$("#modal-jumlah-item #jumlahBeli").val("");
	}

	function hitungSubTotal() {
		var jmlBeli = $("#modal-jumlah-item #jumlahBeli").val();
		var subTotal = $("#modal-jumlah-item #hargaBuku").val() * jmlBeli;
		$("#modal-jumlah-item #subTotal").val(subTotal);
	}

	function loadData() {
		$.ajax({
			url : 'pembelian/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data").html(data);
			}
		});
	}

	$("#modal-search-buku").on("click", ".btn-pilih", function() {

		$("#modal-jumlah-item #idBuku").val($(this).val());
		$("#modal-jumlah-item #judulBuku").val($(this).attr("data-judulBuku"));
		$("#modal-jumlah-item #hargaBuku").val($(this).attr("data-hargaJual"));

		$("#modal-jumlah-item").modal('show');
		$("#modal-search-buku").modal('hide');
	});

	$("#modal-search-supplier").on("click", ".btn-pilih", function() {
		var namaSupplier = $(this).attr("data-nama");
		var noTelepon = $(this).attr("data-telp");
		var noFax = $(this).attr("data-fax");
		var email = $(this).attr("data-email");

		$("#modal-input #namaSupplier").val(namaSupplier);
		$("#modal-input #noTelp").val(noTelepon);
		$("#modal-input #noFaxSupplier").val(noFax);
		$("#modal-input #emailSupplier").val(email);
		$("#modal-input #id").val($(this).val());

		$("#modal-search-supplier").modal('hide');
	});

	function loadSupplier() {
		$.ajax({
			url : 'pembelian/listSupplier.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data-supplier").html(data);
				$("#modal-search-supplier").modal('show');
			}
		});
	}

	function loadBuku() {
		$.ajax({
			url : 'pembelian/listBuku.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data-buku").html(data);
				$("#modal-search-buku").modal('show');
			}
		});
	}

	$("#btn-add").on("click", function() {
		$.ajax({
			url : 'pembelian/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
		$("#label_modal").text("Tambah Pembelian Baru");
	});
</script>
