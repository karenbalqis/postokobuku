<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Tabel Buku</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> Add Buku
			</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>ISBN</th>
					<th>Judul Buku</th>
					<th>Kategori</th>
					<th>Pengarang</th>
					<th>Tahun</th>
					<th>Penerbit</th>
					<th>Jumlah Stok</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal">User</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<script>
	loadData();
	$("#modal-input #penerbitId").change(function(){
		alert("test");
	});

	function loadData() {
		$.ajax({
			url : 'buku/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data").html(data);
			}
		});
	}

	$("#btn-add").on("click", function() {
		$.ajax({
			url : 'buku/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
				addYear();
			}
		});
		$("#label_modal").text("Tambah Buku Baru");
	});

	$("#modal-input").on("submit", "#form-buku", function() {
		$.ajax({
			url : 'buku/save.json',
			type : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(data) {
				if (data.result == "berhasil") {
					$("#modal-input").modal('hide');
					loadData();
				} else {
					$("#modal-input").modal('show');
				}
			}
		});
		return false;
	});

	// button edit di klik
	$("#list-data").on("click", ".btn-edit", function() {
		var vId = $(this).val();
		$.ajax({
			url : 'buku/edit.html',
			type : 'get',
			data : {
				id : vId
			},
			dataType : 'html',
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
		$("#label_modal").text("Edit User Terdaftar");
	});

	// button delete
	$("#list-data").on("click", ".btn-delete", function() {
		var vId = $(this).val();
		$.ajax({
			url : 'buku/delete.html',
			type : 'get',
			data : {
				id : vId
			},
			dataType : 'html',
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
		$("#label_modal").text("Konfirmasi Hapus User");
	});
</script>
