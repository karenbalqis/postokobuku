<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table">
	<thead>
		<tr>
			<th>ISBN</th>
			<th>Judul Buku</th>
			<th>Kategori</th>
			<th>Nama Pengarang</th>
			<th>Tahun Terbit</th>
			<th>Harga Jual</th>
			<th>Penerbit</th>
		</tr>
	</thead>
	<tbody id="list-data-buku">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.isbn}</td>
				<td>${item.judulBuku}</td>
				<td>${item.kategoriModel.nama}</td>
				<td>${item.pengarangModel.namaPengarang}</td>
				<td>${item.tahunTerbit}</td>
				<td>${item.hargaJual}</td>
				<td>${item.penerbitModel.namaPenerbit}</td>
				<td>
					<button type="button"
						class="btn btn-success btn-xs btn-pilih"
						value="${item.id }" data-judulBuku="${item.judulBuku}"
						data-hargaJual="${item.hargaJual}">
						<i class="fa fa-edit"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>