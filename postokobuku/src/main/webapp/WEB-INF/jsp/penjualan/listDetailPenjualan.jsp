<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table" border="1">
	<thead>
		<tr>
			<th>Judul Buku</th>
			<th align="right">Harga Satuan</th>
			<th align="right">Jumlah Buku</th>
			<th align="right">Total Harga</th>
		</tr>
	</thead>
	<tbody id="list-data-pelanggan">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.bukuModel.judulBuku}</td>
				<td align="right">${item.harga}</td>
				<td align="right">${item.jumlah}</td>
				<td align="right">${item.subTotal}</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4" align="right">
				<h3>Total Penjualan: ${item.total}</h3>
			</td>
		</tr>
	</tfoot>
	
</table>