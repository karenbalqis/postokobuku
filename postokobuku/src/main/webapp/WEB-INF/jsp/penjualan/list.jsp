<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.nomorFaktur}</td>
		<td>${item.tanggalBeli}</td>
		<td>${item.pelangganModel.namaPelanggan}</td>
		<td>${item.total}</td>
		
		<td>
			<button type="button" class="btn btn-success btn-xs btn-rincian"
				value="${item.id }">
				<i class="fa fa-file-o"></i>
				Rincian Penjualan
			</button>
		</td>
	</tr>
</c:forEach>