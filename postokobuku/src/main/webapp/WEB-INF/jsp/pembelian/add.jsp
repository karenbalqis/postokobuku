<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-pembelian" class="form-horizontal" action="save"
	method="post">
	<input type="hidden" id="proses" name="proses" class="form-control"
		value="insert">
	<div class="row">
		<div class="col-md-6">
			<input type="hidden" id="supplierId" name="supplierId">
			<div class="form-group">
				<label class="control-label col-md-3">Nama Supplier</label>
				<div class="col-md-8">
					<div class="input-group input-group-sm">
						<input type="text" name="namaSupplier" id="namaSupplier"
							class="form-control" readonly="readonly" /> <span
							class="input-group-btn">
							<button type="button" id="btn-search"
								class="btn btn-info btn-flat">Go!</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Alamat</label>
				<div class="col-md-8">
					<textarea class="form-control" rows="3" readonly="readonly"
						id="alamatSupplier"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nomor Faktur</label>
				<div class="col-md-8">
					<input type="text" name="nomorFaktur" id="nomorFaktur"
						class="form-control" required="required"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Tanggal Pembelian</label>
				<div class="col-md-8">
					<input type="text" class="form-control"
						data-inputmask="'alias': 'yyyy/mm/dd'" data-mask=""
						name="tanggalBeli" id="tanggalBeli" required="required">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-3">No. Telepon</label>
				<div class="col-md-8">
					<input type="text" class="form-control" readonly="readonly"
						id="noTelp" name="noTelp">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">No. Fax</label>
				<div class="col-md-8">
					<input type="text" name="noFaxSupplier" id="noFaxSupplier"
						class="form-control" readonly="readonly" required="required">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-8">
					<input type="text" name="noEmail" id="emailSupplier"
						class="form-control" readonly="readonly">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="tambah-item">Tambah
					Item Buku</button>
			</div>
		</div>
	</div>

	<div class="box box-success" style="min-height: 150px;">
		<div class="box-header">
			<h3 class="box-title">Item-item Buku yang Ditambahkan ke
				Pembelian</h3>
		</div>
		<div class="box-body no-padding">
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Judul Buku</th>
						<th>Harga Satuan</th>
						<th>Jumlah Beli</th>
						<th>Sub Total</th>
					</tr>
				</thead>
				<tbody id="listBeli" style="overflow-y:scroll;">
				</tbody>
			</table>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3">Grand Total</label>
		<div class="col-md-8">
			<input type="text" class="form-control" readonly="readonly"
				id="grandTotal" name="grandTotal">
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Simpan
			Pembelian</button>
	</div>
</form>