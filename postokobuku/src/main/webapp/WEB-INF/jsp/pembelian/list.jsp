<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.nomorFaktur}</td>
		<td>${item.supplierModel.namaSupplier}</td>
		<td>${item.total}</td>
		<td>${item.tanggalBeli}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-rincian"
				value="${item.id }">
				<i class="fa fa-file-o"></i>
				Rincian Pembelian
			</button>
		</td>
	</tr>
</c:forEach>