<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<table class="table">
	<thead>
		<tr>
			<th>Nama Supplier</th>
			<th>No Telepon</th>
			<th>No Faximile</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody id="list-data-supplier">
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.namaSupplier}</td>
				<td>${item.noTelepon}</td>
				<td>${item.noFax}</td>
				<td>${item.email}</td>
				<td>
					<button type="button"
						class="btn btn-success btn-xs btn-edit btn-pilih"
						value="${item.id }" data-nama="${item.namaSupplier}"
						data-telp="${item.noTelepon}" data-fax="${item.noFax}"
						data-email="${item.email}"
						data-alamat="${item.alamat}">
						<i class="fa fa-check"></i>
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>