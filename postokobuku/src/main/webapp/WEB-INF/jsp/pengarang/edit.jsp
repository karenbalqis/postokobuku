<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-pengarang" action="save" method="post">
	<input type="hidden" id="id" name="id" value="${item.id}"> <input
		type="hidden" id="proses" name="proses" value="update">
	<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-md-2">Nama Pengarang</label>
			<div class="col-md-10">
				<input type="text" id="namaPengarang" name="namaPengarang"
					class="form-control" required="required" maxlength="60"
					value="${item.namaPengarang}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Alamat</label>
			<div class="col-md-10">
				<input type="text" id="alamat" name="alamat" class="form-control"
					required="required" maxlength="100" value="${item.alamat }">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Provinsi</label>
			<div class="col-md-10">
				<select class="form-control" id="provinsiId" name="provinsiId"
					class="form-control" required="required">
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Kota/Kab.</label>
			<div class="col-md-10">
				<select class="form-control" id="kotaId" name="kotaId"
					class="form-control" required="required">
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Kecamatan</label>
			<div class="col-md-10">
				<select class="form-control" id="kecamatanId" name="kecamatanId"
					class="form-control" required="required">
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Nomor Telepon</label>
			<div class="col-md-10">
				<input type="text" id="noTelepon" name="noTelepon"
					class="form-control" required="required" maxlength="15"
					value="${item.noTelepon}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Email</label>
			<div class="col-md-10">
				<input type="email" id="email" name="email" class="form-control"
					required="required" maxlength="60" value="${item.email}">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
<script>
	//Load state
	function loadProvinsi(selectedId) {
		$.ajax({
			url : 'ajax/getProvinsi.json',
			type : 'get',
			dataType : 'json',
			success : function(data) {
				//Kosongkan provinsi, kota dan kecamatan
				$("#provinsiId").empty();
				$("#kotaId").empty();
				$("#kecamatanId").empty();

				//Isi provinsi dan kota dengan value kosong
				$("#modal-input #provinsiId").append(
						'<option value="0">Silahkan Pilih</option>');
				$("#modal-input #kotaId").append(
						'<option value="0">Pilih Provinsi Dulu</option>');
				$("#modal-input #kecamatanId").append(
						'<option value="0">Pilih Provinsi Dulu</option>');

				//Looping berdasarkan provinsi yang didapat
				$.each(data.result, function(index, item) {

					if (selectedId == item.id) {
						$("#modal-input #provinsiId").append(
								'<option value="'+item.id+'" selected="selected">'
										+ item.nama + '</option>');
					} else {
						$("#modal-input #provinsiId").append(
								'<option value="'+item.id+'">' + item.nama
										+ '</option>');
					}

				});
			}
		});
	}

	function loadKota(id, selectedId) {
		$.ajax({
			url : 'ajax/getKotaByProvinsiId.json',
			type : 'get',
			dataType : 'json',
			data : {
				id : id
			},
			success : function(data) {
				//Kosongkan kota dan kecamatan
				$("#kotaId").empty();
				$("#modal-input #kecamatanId").empty();

				$("#modal-input #kotaId").append(
						'<option value="0">Silahkan Pilih</option>');
				$("#modal-input #kecamatanId").append(
						'<option value="0">Pilih Kota Dulu</option>');

				$.each(data.result, function(index, item) {
					if (item.id == selectedId) {
						$("#modal-input #kotaId").append(
								'<option value="'+item.id+'" selected="selected">'
										+ item.nama + '</option>');
					} else {
						$("#modal-input #kotaId").append(
								'<option value="'+item.id+'">' + item.nama
										+ '</option>');
					}

				});
			}
		});
	}

	function loadKecamatan(id, selectedId) {
		$.ajax({
			url : 'ajax/getKecamatanByKotaId.json',
			type : 'get',
			dataType : 'json',
			data : {
				id : id
			},
			success : function(data) {
				//Kosongkan kecamatan
				$("#modal-input #kecamatanId").empty();

				//Isi kecamatan dengan value 0
				$("#modal-input #kecamatanId").append(
						'<option value="0">Silahkan Pilih</option>');

				$.each(data.result, function(index, item) {
					if (item.id == selectedId) {
						$("#modal-input #kecamatanId").append(
								'<option value="'+item.id+'" selected="selected">'
										+ item.nama + '</option>');
					} else {
						$("#modal-input #kecamatanId").append(
								'<option value="'+item.id+'">' + item.nama
										+ '</option>');
					}
				});
			}
		});
	}
	//End of load state

	$(document).ready(function() {
		loadProvinsi("${item.provinsiId}");
		loadKota("${item.provinsiId}", "${item.kotaId}");
		loadKecamatan("${item.kotaId}", "${item.kecamatanId}");
	});
</script>