<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Tabel Pembelian</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> Add Pembelian
			</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Nomor Faktur</th>
					<th>Supplier</th>
					<th>Total Pembelian</th>
					<th>Waktu Pembelian</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal" style="overflow-y: auto;">
	<div class="modal-dialog" style="width: 95%; min-height: 95%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal"></h4>
			</div>
			<div class="modal-body"></div>

		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-supplier" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Supplier</h4>

			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-buku" class="modal">
	<div class="modal-dialog" style="width: 95%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Buku</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-detail-pembelian" class="modal">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Rincian Pembelian</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<script>
	function loadData() {
		$.ajax({
			url : 'pembelian/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data").html(data);
			}
		});
	}

	function loadSupplier() {
		$.ajax({
			url : 'pembelian/listSupplier.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-search-supplier .modal-body").html(data);
				$("#modal-search-supplier").modal('show');
			}
		});
	}

	function loadBuku() {
		$.ajax({
			url : 'pembelian/listBuku.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-search-buku .modal-body").html(data);
				$("#modal-search-buku").modal('show');
			}
		});
	}

	loadData();
	//Tambah Pembelian
	$("#btn-add").on("click", function() {
		$.ajax({
			url : 'pembelian/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-input").find(".modal-body").html(data);
				$("#modal-input").modal('show');
			}
		});
		$("#label_modal").text("Tambah Pembelian Baru");
	});

	//Cari Supplier
	$("#modal-input").on("click", "#btn-search", function() {
		loadSupplier();
	});

	//Pilih Supplier
	$("#modal-search-supplier").on("click", ".btn-pilih", function() {
		var namaSupplier = $(this).attr("data-nama");
		var noTelepon = $(this).attr("data-telp");
		var noFax = $(this).attr("data-fax");
		var email = $(this).attr("data-email");
		var alamat  = $(this).attr("data-alamat");

		$("#modal-input #namaSupplier").val(namaSupplier);
		$("#modal-input #noTelp").val(noTelepon);
		$("#modal-input #noFaxSupplier").val(noFax);
		$("#modal-input #emailSupplier").val(email);
		$("#modal-input #supplierId").val($(this).val());
		$("#modal-input #alamatSupplier").val(alamat);

		$("#modal-search-supplier").modal('hide');
	});

	//Tambah Buku
	$("#modal-input").on("click", "#tambah-item", function() {
		loadBuku();
	});

	$("#modal-search-buku")
			.on(
					"click",
					".btn-pilih",
					function() {
						var index = $("#modal-input #listBeli >tr").length;
						var tr = '<tr class="item-buku">'
								+ '<td><input type="hidden" name="detailPembelians['
								+ index
								+ '].bukuId" value="'
								+ $(this).val()
								+ '">'
								+ $(this).attr("data-judulBuku")
								+ '</td>'
								+ '<td><input type="text" name="detailPembelians['
								+ index
								+ '].harga"  readonly="readonly" value="'
								+ $(this).attr("data-hargaBeli")
								+ '" class="form-control hargaBeli"></td>'
								+ '<td><input type="text" name="detailPembelians['+index+'].jumlah" value="" class="form-control jumlahBeli"></td>'
								+ '<td><input type="text" name="detailPembelians['+index+'].subTotal" value="" class="form-control subTotal" readonly="readonly"></td>'
								+ '<td><button type="button" class="btn btn-block btn-danger btn-xs hapus-item">Hapus</button></td>'
								+ '</tr>';

						$("#modal-input #listBeli").append(tr);
						$("#modal-search-buku").modal('hide');
					});
	
	//Ubah Jumlah Beli
	var grandTotal = 0;
	$("#modal-input").on("keyup", ".jumlahBeli", function() {
		var jumlahBeli = $(this).val();
		var hargaBeli = $(this).parent().parent().find(".hargaBeli").val();
		var subTotal = jumlahBeli * hargaBeli;
		
		grandTotal 	= grandTotal+subTotal;

		$(this).parent().parent().find(".subTotal").val(subTotal);
		$("#modal-input #grandTotal").val(grandTotal);
	});
	
	//Hapus Item Pembelian
	$("#modal-input").on("click", ".hapus-item", function(){
		var subTotal = $(this).parent().parent().find(".subTotal").val();
		grandTotal 	= grandTotal-subTotal;
		
		$("#modal-input #grandTotal").val(grandTotal);
		$(this).parent().parent().html("");
	});

	//Simpan Pembelian
	$("#modal-input").on("submit", "#form-pembelian", function() {
		$.ajax({
			url			: 'pembelian/save.json',
			type		: 'post',
			data		: $(this).serialize(),
			dataType 	: 'json',
			success 	: function(data) {
				if (data.result == "berhasil") {
					$("#modal-input").modal('hide');
					alert(data.result);
					loadData();
				} else {
					$("#modal-input").modal('show');
				}
			}
		});
		return false;
	});
	
	//Lihat Rincian Pembelian
	$("#list-data").on("click", ".btn-rincian", function(){
		var detailPembelianId = $(this).val();
		
		$.ajax({
			url			: 'pembelian/detailPembelian.html',
			type 		: 'get',
			data 		: {id:detailPembelianId},
			dataType 	: 'html',
			success		: function(data){
				$("#modal-detail-pembelian").find(".modal-body").html(data);
				$("#modal-detail-pembelian").modal("show");
			}
		});
	});
</script>
