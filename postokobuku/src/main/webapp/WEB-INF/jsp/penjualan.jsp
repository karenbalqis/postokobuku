<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Penjualan</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right"> <i class="fa fa-plus"></i>Penjualan Baru</button>
		</div>
	</div>
	
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>No Faktur</th>
					<th>Tanggal transaksi penjualan</th>
					<th>Pelanggan</th>
					<th>Grand Total</th>
					
			</thead>
			<tbody id="list-data">
			
			</tbody>
			
		</table>
	</div>
</div>

<!-- - Modal -->
<div id="modal-input" class="modal" style="overflow-y: auto;">
	<div class="modal-dialog" style="width : 95%">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4></h4>
			</div>
			<div class="modal-body">
			
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog" style="width : 95%" min-height:95%>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal"></h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-search-pelanggan" class="modal">
	<div class="modal-dialog" style="width:95%">
		<div class="modal-content" style="margin:auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Pelanggan</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
	
</div>

<!-- Modal -->
<div id="modal-search-buku" class="modal">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content" style="margin:auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Daftar Buku</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-detail-penjualan" class="modal">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content" style="margin: auto;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Rincian Penjualan</h4>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>
</div>

<script>

function loadData() {
	$.ajax({
		url : 'penjualan/list.html',
		type : 'get',
		dataType : 'html',
		success : function(data){
			$('#list-data').html(data);
		}
	});
}

function loadPelanggan(){
	$.ajax({
		url : 'penjualan/listPelanggan.html',
		type : 'get',
		dataType : 'html',
		success : function(data){
			$("#modal-search-pelanggan .modal-body").html(data);
			$("#modal-search-pelanggan").modal('show');
		}
	});
}

function loadBuku(){
	$.ajax({
		url : 'penjualan/listBuku.html',
		type : 'get',
		dataType : 'html',
		success : function(data){
			$("#modal-search-buku .modal-body").html(data);
			$("#modal-search-buku").modal('show');
		}
	})
}


loadData();
//event
$("#btn-add").on("click", function(){
	$.ajax({
		url : 'penjualan/add.html',
		type : 'get',
		dataType : 'html',
		success : function(data){
			$("#modal-input").find(".modal-body").html(data);
			$("#modal-input").modal('show');
		}
	});
	$("#label_modal").text("Tambah Penjualan Baru");
});

//cari pelanggan
$("#modal-input").on("click", "#btn-search" , function(){
	loadPelanggan();
});

//Pilih Pelanggan
$("#modal-search-pelanggan").on("click", ".btn-pilih", function(){
	var namaPelanggan = $(this).attr("data-nama");
	var noTelepon = $(this).attr("data-telp");
	var email = $(this).attr("data-email");
	var alamat = $(this).attr("data-alamat");
	
	$("#modal-input #namaPelanggan").val(namaPelanggan);
	$("#modal-input #noTelp").val(noTelepon);
	$("#modal-input #emailPelanggan").val(email);
	$("#modal-input #pelangganId").val($(this).val());
	$("#modal-input #alamatPelanggan").val(alamat); 
	
	$("#modal-search-pelanggan").modal('hide');
});

//Tambah buku
$("#modal-input").on("click", "#tambah-item", function(){
	loadBuku();
});

$("#modal-search-buku")
.on(
		"click",
		".btn-pilih",
		function() {
			var index = $("#modal-input #listJual >tr").length;
			var tr = '<tr class="item-buku">'
				+ '<td><input type="hidden" name="penjualanDetailModels['
				+ index
				+ '].bukuId" value="'
				+ $(this).val()
				+ '">'
				+ $(this).attr("data-judulBuku")
				+ '</td>'
				+ '<td><input type="text" name="penjualanDetailModels['
				+ index
				+ '].harga"  readonly="readonly" value="'
				+ $(this).attr("data-hargaJual")
				+ '" class="form-control hargaJual"></td>'
				+ '<td><input type="text" name="penjualanDetailModels['+index+'].jumlah" value="" class="form-control jumlahJual"></td>'
				+ '<td><input type="text" name="penjualanDetailModels['+index+'].subTotal" value="" class="form-control subTotal" readonly="readonly"></td>'
				+ '<td><button type="button" class="btn btn-block btn-danger btn-xs hapus-item">Hapus</button></td>'
				+ '</tr>';
				
			$("#modal-input #listJual").append(tr);
			$("#modal-search-buku").modal('hide');
		});
		
		var grandTotal = 0;
		
		$("#modal-input").on("keyup" , ".jumlahJual" , function(){
			var jumlahJual = $(this).val();
			var hargaJual = $(this).parent().parent().find(".hargaJual").val();
			var subTotal =  jumlahJual * hargaJual ;
			grandTotal = grandTotal + subTotal;
			
			$(this).parent().parent().find(".subTotal").val(subTotal);
			$("#modal-input #grandTotal").val(grandTotal);
		});

		//Hapus Item Penjualan
		$("#modal-input").on("click", ".hapus-item", function(){
			var subTotal = $(this).parent().parent().find(".subTotal").val();
			grandTotal = grandTotal - subTotal;
			
			$("#modal-input #grandTotal").val(grandTotal);
			$(this).parent().parent().html("");
		});
		
		//Simpan Penjualan
		$("#modal-input").on("submit", "#form-penjualan" ,function(){
			$.ajax({
				url		: 'penjualan/save.json',
				type	: 'post',
				data	: $(this).serialize(),
				dataType : 'json',
				success  : function(data){
					if(data.result == 'berhasil'){
						$("#modal-input").modal('hide');
						alert(data.result);
						loadData();
					} else {
						$("#modal-input").modal('show');
					}
				}
			});
			return false;
		});
		
		
		
		//Lihat Rincian Penjualan
		$("#list-data").on("click", ".btn-rincian", function(){
			var detailPenjualanId = $(this).val();
			
			$.ajax({
				url			: 'penjualan/detailPenjualan.html',
				type 		: 'get',
				data 		: {id:detailPenjualanId},
				dataType 	: 'html',
				success		: function(data){
					$("#modal-detail-penjualan").find(".modal-body").html(data);
					$("#modal-detail-penjualan").modal("show");
				}
			});
		});
		
		//reset nilai total penjualan ketika modal dihide
	
		
</script>

