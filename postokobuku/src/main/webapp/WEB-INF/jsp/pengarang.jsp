<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Tabel Pengarang</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right">
				<i class="fa fa-plus"></i> Add Pengarang
			</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Nama Pengarang</th>
					<th>No Telepon</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody id="list-data">

			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="modal-input" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4 id="label_modal">User</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<script>
	//change handler input
	$("#modal-input").on("change", "#provinsiId", function() {
		var id = $(this).val();
		loadKota(id);
	});

	$("#modal-input").on("change", "#kotaId", function() {
		var id = $(this).val();
		loadKecamatan(id);
	});

	//End of change handler input

	//Load state
	function loadProvinsi(selectedId) {
		$.ajax({
			url : 'ajax/getProvinsi.json',
			type : 'get',
			dataType : 'json',
			success : function(data) {
				//Kosongkan provinsi, kota dan kecamatan
				$("#provinsiId").empty();
				$("#kotaId").empty();
				$("#kecamatanId").empty();

				//Isi provinsi dan kota dengan value kosong
				$("#modal-input #provinsiId").append(
						'<option value="0">Silahkan Pilih</option>');
				$("#modal-input #kotaId").append(
						'<option value="0">Pilih Provinsi Dulu</option>');
				$("#modal-input #kecamatanId").append(
						'<option value="0">Pilih Provinsi Dulu</option>');

				//Looping berdasarkan provinsi yang didapat
				$.each(data.result, function(index, item) {

					//Isikan list provinsi pada:
					//#id-modal 	#id-dropdown
					$("#modal-input #provinsiId").append(
							'<option value="'+item.id+'" >' + item.nama
									+ '</option>');
				});
			}
		});
	}

	function loadKota(id) {
		$.ajax({
			url : 'ajax/getKotaByProvinsiId.json',
			type : 'get',
			dataType : 'json',
			data : {
				id : id
			},
			success : function(data) {
				//Kosongkan kota dan kecamatan
				$("#kotaId").empty();
				$("#kecamatanId").empty();

				$("#kotaId").append(
						'<option value="0">Silahkan Pilih</option>');
				$("#kecamatanId").append(
						'<option value="0">Pilih Kota Dulu</option>');

				$.each(data.result, function(index, item) {
					$("#kotaId").append(
							'<option value="'+item.id+'">' + item.nama
									+ '</option>');
				});
			}
		});
	}

	function loadKecamatan(id) {
		$.ajax({
			url : 'ajax/getKecamatanByKotaId.json',
			type : 'get',
			dataType : 'json',
			data : {
				id : id
			},
			success : function(data) {
				//Kosongkan kecamatan
				$("#modal-input #kecamatanId").empty();

				//Isi kecamatan dengan value 0
				$("#modal-input #kecamatanId").append(
						'<option value="0">Silahkan Pilih</option>');

				$.each(data.result, function(index, item) {
					$("#modal-input #kecamatanId").append(
							'<option value="'+item.id+'">' + item.nama
									+ '</option>');
				});
			}
		});
	}
	//End of load state

	function loadData() {
		$.ajax({
			url : 'pengarang/list.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#list-data").html(data);
			}
		});
	}

	loadData();

	$(document).ready(function() {

		$("#btn-add").on("click", function() {
			$.ajax({
				url : 'pengarang/add.html',
				type : 'get',
				dataType : 'html',
				success : function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
			$("#label_modal").text("Tambah Pengarang Baru");
			loadProvinsi(null);
		});

		$("#modal-input").on("submit", "#form-pengarang", function() {
			$.ajax({
				url : 'pengarang/save.json',
				type : 'post',
				data : $(this).serialize(),
				dataType : 'json',
				success : function(data) {
					if (data.result == "berhasil") {
						$("#modal-input").modal('hide');
						loadData();
					} else {
						$("#modal-input").modal('show');
					}
				}
			});
			return false;
		});

		// button edit di klik
		$("#list-data").on("click", ".btn-edit", function() {
			var vId = $(this).val();
			$.ajax({
				url : 'pengarang/edit.html',
				type : 'get',
				data : {
					id : vId
				},
				dataType : 'html',
				success : function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
					
					//Tampilkan data provinsi
					loadProvinsi();
				}
			});
			$("#label_modal").text("Edit Pengarang");
		});

		// button delete
		$("#list-data").on("click", ".btn-delete", function() {
			var vId = $(this).val();
			$.ajax({
				url : 'pengarang/delete.html',
				type : 'get',
				data : {
					id : vId
				},
				dataType : 'html',
				success : function(data) {
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
			$("#label_modal").text("Konfirmasi Hapus Pengarang");
		});
	});
</script>
