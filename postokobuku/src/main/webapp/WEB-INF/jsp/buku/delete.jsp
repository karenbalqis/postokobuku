<form id="form-buku" action="update" method="post">
	<div class="form-horizontal">
	<!-- Tarik data dengan variabel item yang dikirim controller, simpan pada form hidden -->
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="hidden" id="id" name="id" value="${item.id}">
		<input type="hidden" id="judulBuku" name="judulBuku" value="${item.judulBuku}">
		<input type="hidden" id="isbn" name="isbn" value="${item.isbn}">
		<input type="hidden" id="kategoriId" name="kategoriId" value="${item.kategoriId}">
		<input type="hidden" id="pengarangId" name="pengarangId" value="${item.pengarangId}">
		<input type="hidden" id="tahunTerbit" name="tahunTerbit" value="${item.tahunTerbit}">
		<input type="hidden" id="kotaId" name="kotaId" value="${item.kotaId}">
		<input type="hidden" id="penerbitId" name="penerbitId" value="${item.penerbitId}">
		<input type="hidden" id="jumlahHalaman" name="jumlahHalaman" value="${item.jumlahHalaman}">
		<div class="form-group">
			<div class="col-md-12">
				<p>Buku dengan judul <b>${item.judulBuku}</b> akan dihapus, lanjutakan?</p>
			</div>

		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-danger">Lanjutkan Hapus</button>
	</div>
</form>
