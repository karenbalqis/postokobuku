<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-buku" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ISBN</label>
			<div class="col-md-10">
				<input type="text" id="isbn" name="isbn" class="form-control"
					required="required" maxlength="60">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Judul Buku</label>
			<div class="col-md-10">
				<input type="text" id="judulBuku" name="judulBuku"
					class="form-control" required="required" maxlength="60">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Kategori</label>
			<div class="col-md-10">
				<select class="form-control" id="kategoriId" name="kategoriId"
					class="form-control" required="required">
					<option value="">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Pengarang</label>
			<div class="col-md-10">
				<select class="form-control" id="pengarangId" name="pengarangId"
					class="form-control" required="required">
					<option value="">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Tahun Terbit</label>
			<div class="col-md-10">
				<select class="form-control" id="tahunTerbit" name="tahunTerbit"
					class="form-control" required="required">
					<option value="">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Jumlah Halaman</label>
			<div class="col-md-10">
				<input type="text" id="jumlahHalaman" name="jumlahHalaman"
					class="form-control" required="required" maxlength="3">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Nama Penerbit</label>
			<div class="col-md-10">
				<select class="form-control" id="penerbitId" name="penerbitId"
					class="form-control" required="required">
					<option value="0" data-kotaid="">Nyusul</option>
				</select>
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label col-md-2">Harga Beli</label>
			<div class="col-md-10">
				<input type="text" id="hargaBeli" name="hargaBeli"
					class="form-control" required="required" maxlength="60">
			</div>
		</div>				
		<div class="form-group">
			<label class="control-label col-md-2">Harga Jual</label>
			<div class="col-md-10">
				<input type="text" id="hargaJual" name="hargaJual"
					class="form-control" required="required" maxlength="60">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-10">
				<input type="hidden" id="kotaId" name="kotaId" class="form-control"
					required="required" maxlength="60" value="1101">
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
<script>
	$(document).ready(function() {
		loadKategori();
		loadPengarang();
		addYear();
		loadPenerbit();
	});

	function addYear() {
		var tahun = (new Date()).getFullYear();
		$("#modal-input #tahunTerbit").empty();
		$("#modal-input #tahunTerbit")
				.append('<option>Silahkan Pilih</option>');
		for (var i = 0; i <= 20; i++) {
			$("#modal-input #tahunTerbit").append(
					'<option value="' + (tahun - i) + '">' + (tahun - i)
							+ '</option>');
		}
	}
	
	function loadPenerbit() {
		$.ajax({
			url : 'ajax/getPenerbit.json',
			type : 'get',
			dataType : 'json',
			success : function(data) {
				//Kosongkan penerbit
				$("#modal-input #penerbitId").empty();

				//Isi penerbit dengan value 0
				$("#modal-input #penerbitId").append(
						'<option value="0">Silahkan Pilih</option>');

				$.each(data.result, function(index, item) {
					$("#modal-input #penerbitId").append(
							'<option value="'+item.id+'" data-kotaid="'+item.kotaId+'">' + item.namaPenerbit
									+ '</option>');
				});
			}
		});
	}

	function loadPengarang() {
		$.ajax({
			url : 'ajax/getPengarang.json',
			type : 'get',
			dataType : 'json',
			success : function(data) {
				//Kosongkan pengarang
				$("#modal-input #pengarangId").empty();

				//Isi pengarang dengan value 0
				$("#modal-input #pengarangId").append(
						'<option value="0">Silahkan Pilih</option>');

				$.each(data.result, function(index, item) {
					$("#modal-input #pengarangId").append(
							'<option value="'+item.id+'">' + item.namaPengarang
									+ '</option>');
				});
			}
		});
	}

	function loadKategori() {
		$.ajax({
			url : 'ajax/getKategori.json',
			type : 'get',
			dataType : 'json',
			success : function(data) {
				//Kosongkan kategori
				$("#modal-input #kategoriId").empty();

				//Isi kategori dengan value 0
				$("#modal-input #kategoriId").append(
						'<option value="0">Silahkan Pilih</option>');

				$.each(data.result, function(index, item) {
					$("#modal-input #kategoriId").append(
							'<option value="'+item.id+'" data-kotaId="'+item.kotaId+'">' + item.nama
									+ '</option>');
				});
			}
		});
	}
</script>