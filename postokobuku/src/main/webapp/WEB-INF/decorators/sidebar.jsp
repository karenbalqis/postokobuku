<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${contextName}/assets/dist/img/user2-160x160.jpg"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>${username}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">NAVIGASI MENU</li>
			<li class="treeview"><a href="#"> <i class="fa fa-users"></i>
					<span>User</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/role.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Role User</a></li>
					<li><a href="${contextName}/user.html" class="menu-item"><i
							class="fa fa-circle-o"></i> User Terdaftar</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-book"></i>
					<span>Buku</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/kategori.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Kategori Buku</a></li>
					<li><a href="${contextName}/pengarang.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Pengarang</a></li>
					<li><a href="${contextName}/penerbit.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Penerbit</a></li>
					<li><a href="${contextName}/buku.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Buku</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-chain"></i>
					<span>Mitra</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/supplier.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Supplier</a></li>
					<li><a href="${contextName}/pelanggan.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Pelanggan</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-money"></i>
					<span>Transaksi</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/pembelian.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Pembelian</a></li>
					<li><a href="${contextName}/penjualan.html" class="menu-item"><i
							class="fa fa-circle-o"></i> Penjualan</a></li>
				</ul></li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>